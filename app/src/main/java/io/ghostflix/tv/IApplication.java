package io.ghostflix.tv;

public interface IApplication {

    int getImageCardViewContentTextResId();
    int getImageCardViewInfoFieldResId();
    int getImageCardViewTitleTextResId();
}

package io.ghostflix.tv.fragmentui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.user.UserContentResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.ui.LoginActivity;
import io.ghostflix.tv.utils.SessionManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProfileFragment extends Fragment {


    private SwimlaneFragment mSwimlaneFragment;
    private TextView mTextViewTitle;
    private ImageView mUserImageView;
    private Button mButtonLogout;
    private Button mPrivacyButton;
    private Button mImprintButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mSwimlaneFragment = new SwimlaneFragment();
        View view = inflater.inflate(R.layout.profile_layout, container, false);
        mTextViewTitle = view.findViewById(R.id.user_title);
        mUserImageView = view.findViewById(R.id.user_imageView);
        mButtonLogout = view.findViewById(R.id.logout);
        mPrivacyButton = view.findViewById(R.id.privacy_button);
        mImprintButton = view.findViewById(R.id.imprint_button);
        mButtonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Logout")
                        .setMessage("Bist du sicher, dass du dich ausloggen willst?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                SessionManager sessionManager = new SessionManager(getContext());
                                sessionManager.clearSharedPref();
                                clearCache();
                                Intent i = new Intent(getContext(), LoginActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);

                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
        mPrivacyButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToPrivacy();
            }
        });

        mImprintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToImprint();
            }
        });
        replaceFragment();
        getUserContent();
        return view;
    }

    private void replaceFragment() {
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.profile_layout_fragment, mSwimlaneFragment, "profile");
        ft.commit();
    }

    private void goToPrivacy() {
        PrivacyFragment privacyFragment= new PrivacyFragment();
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.home_container, privacyFragment, "privacy");
        ft.commit();
    }

    private void goToImprint() {
        ImprintFragment privacyFragment= new ImprintFragment();
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.home_container, privacyFragment, "imprint");
        ft.commit();
    }

    private void getUserContent() {

        Retrofit apiClient = ApiClient.getClient();

        SessionManager sessionManager = new SessionManager(getContext());
        Call<UserContentResult> service = apiClient.create(Service.class).getUserData( sessionManager.getToken());
        service.enqueue(new Callback<UserContentResult>() {
            @Override
            public void onResponse(Call<UserContentResult> call, Response<UserContentResult> response) {

                if(response.body()!=null){
                    mTextViewTitle.setText(response.body().getName());
                    Glide.with(getActivity())
                            .load(response.body().getAvatar())
                            .transform(new CircleCrop())
                            .into(mUserImageView);

                    mSwimlaneFragment.setListContent(response.body());
                }
            }
            @Override
            public void onFailure(Call<UserContentResult> call, Throwable t) {


            }
        });
    }

    public void clearCache(){
        Log.d("msg","Clear cache");
        File cacheDirectory = this.getActivity().getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    public boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }

}

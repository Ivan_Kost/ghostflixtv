package io.ghostflix.tv.fragmentui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import io.ghostflix.tv.R;
import io.ghostflix.tv.utils.SessionManager;

public class PrivacyNotifyFragment extends DialogFragment {

    int mNum;

    // Create a new instance of MyDialogFragment, providing "num" as an argument.
    static PrivacyNotifyFragment newInstance(int num) {
        PrivacyNotifyFragment f = new PrivacyNotifyFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments().getInt("num");

        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        switch ((mNum-1)%6) {
            case 1: style = DialogFragment.STYLE_NO_TITLE; break;
            case 2: style = DialogFragment.STYLE_NO_FRAME; break;
            case 3: style = DialogFragment.STYLE_NO_INPUT; break;
            case 4: style = DialogFragment.STYLE_NORMAL; break;
            case 5: style = DialogFragment.STYLE_NO_TITLE; break;
            case 6: style = DialogFragment.STYLE_NO_TITLE; break;
            case 7: style = DialogFragment.STYLE_NO_FRAME; break;
            case 8: style = DialogFragment.STYLE_NORMAL; break;
        }
        switch ((mNum-1)%6) {
            case 4: theme = android.R.style.Theme_Holo; break;
            case 5: theme = android.R.style.Theme_Holo_Light_Dialog; break;
            case 6: theme = android.R.style.Theme_Holo_Light; break;
            case 7: theme = android.R.style.Theme_Holo_Light_Panel; break;
            case 8: theme = android.R.style.Theme_Holo_Light; break;
        }
        style = DialogFragment.STYLE_NO_TITLE;
        setStyle(style, theme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.privacy_notify, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set DialogFragment title
      //  getDialog().setTitle("Dialog #" + mNum);
        view.findViewById(R.id.policy_notify_butt_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissNotification();
            }
        });
        view.findViewById(R.id.policy_notify_butt_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissNotification();
            }
        });
        view.findViewById(R.id.policy_notify_butt_3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissNotification();
            }
        });
    }

    private void dismissNotification(){
        SessionManager sessionManager = new SessionManager(getActivity().getApplicationContext());
        sessionManager.setNotifyIsShown();
        this.dismiss();
    }
}

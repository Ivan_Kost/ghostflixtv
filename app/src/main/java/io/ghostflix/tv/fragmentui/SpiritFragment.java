package io.ghostflix.tv.fragmentui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import java.util.List;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.content.ContentResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.utils.SessionManager;

public class SpiritFragment extends HomeFragment{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSwimlaneFragment = new SwimlaneFragment();
        mSwimlaneFragment.setupSpiritEventListeners();
        mSessionManager = new SessionManager(getActivity());
        apiClient = ApiClient.getClient();
        if (mSwimlaneFragment != null) {
            mSwimlaneFragment.setFavouriteItemViewSelectedListener(this);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.spirit_fragment, container, false);
        mTextViewTittle = view.findViewById(R.id.title);
        mTextViewDesc = view.findViewById(R.id.description);
        mImageView = view.findViewById(R.id.background_image);
        rating = view.findViewById(R.id.rating);
        replaceFragment();
        return view;
    }

    public void setListContent(List<ContentResult> contentResultList) {
        this.contentResultList = contentResultList;
        if(this.top10ResultList != null){
            this.contentResultList.add(0,this.top10ResultList);
            mSwimlaneFragment.setSpiritSwimLaneContent(contentResultList);
        }

    }

    public void setTopListContent(ContentResult topResultList) {
        this.top10ResultList = topResultList;
        if(this.contentResultList != null){
            this.contentResultList.add(0,this.top10ResultList);
            mSwimlaneFragment.setSpiritSwimLaneContent(contentResultList);
        }


    }

}

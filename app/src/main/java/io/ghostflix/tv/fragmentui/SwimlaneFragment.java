package io.ghostflix.tv.fragmentui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.app.RowsSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.FocusHighlight;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;

import io.ghostflix.tv.model.content.ContentResult;
import io.ghostflix.tv.model.content.VideoItem;
import io.ghostflix.tv.model.user.UserContentResult;
import io.ghostflix.tv.presenter.CardPresenter;
import io.ghostflix.tv.presenter.CustomListRowPresenter;
import io.ghostflix.tv.presenter.SpiritCardPresenter;
import io.ghostflix.tv.ui.DetailActivity;
import io.ghostflix.tv.ui.SpiritDetailActivity;
import io.ghostflix.tv.utils.SessionManager;

import java.util.List;
import java.util.Objects;

public class SwimlaneFragment extends RowsSupportFragment {
    private ArrayObjectAdapter mCategoryRowAdapter;
    private final String TAG = "HomeFragment.class";
    private CardPresenter cardPresenter;
    private FragmentActivity context;
    private SpiritCardPresenter spiritCardPresenter;
    private DialogFragment policyDialogFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupUIElements();
       // setupEventListeners();
        context = getActivity();
        SessionManager sessionManager = new SessionManager(context.getApplicationContext());
        if(!sessionManager.isNotifyShown()){
            showDialog();
        }

    }

    protected void setupEventListeners() {
        setOnItemViewClickedListener(new ItemViewClickedListener());
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
    }

    protected void setupSpiritEventListeners() {
        setOnItemViewClickedListener(new SpiritItemViewClickedListener());
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
    }


    private void setupUIElements() {
//        setHeadersState(HEADERS_DISABLED);
//        setHeadersTransitionOnBackEnabled(true);
    }

    public void setSwimLaneContent(List<ContentResult> videoResult) {

        CustomListRowPresenter customListRowPresenter = new CustomListRowPresenter(FocusHighlight.ZOOM_FACTOR_LARGE, false);
        customListRowPresenter.setShadowEnabled(false);
        mCategoryRowAdapter = new ArrayObjectAdapter(customListRowPresenter);
        cardPresenter = new CardPresenter();

        for ( int i = 0 ; i< videoResult.size() ; i++) {

            ContentResult category = videoResult.get(i);
            HeaderItem cardPresenterHeader = new HeaderItem(i, category.getTitle());
            ArrayObjectAdapter cardRowAdapter ;
            cardRowAdapter = new ArrayObjectAdapter(cardPresenter);
            if( category.getItems().size() > 0){
                cardRowAdapter.addAll(0, category.getItems());
                mCategoryRowAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
            }

        }

        setAdapter(mCategoryRowAdapter);
    }

    public void setSpiritSwimLaneContent(List<ContentResult> videoResult) {

        CustomListRowPresenter customListRowPresenter = new CustomListRowPresenter(FocusHighlight.ZOOM_FACTOR_LARGE, false);
        customListRowPresenter.setShadowEnabled(false);
        customListRowPresenter.setSpiritColors();
        mCategoryRowAdapter = new ArrayObjectAdapter(customListRowPresenter);
        spiritCardPresenter = new SpiritCardPresenter();


        for ( int i = 0 ; i< videoResult.size() ; i++) {

            ContentResult category = videoResult.get(i);
            HeaderItem cardPresenterHeader = new HeaderItem(i, category.getTitle());
            ArrayObjectAdapter cardRowAdapter ;
            cardRowAdapter = new ArrayObjectAdapter(spiritCardPresenter);
            if( category.getItems().size() > 0){
                cardRowAdapter.addAll(0, category.getItems());
                mCategoryRowAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
            }

        }

        setAdapter(mCategoryRowAdapter);
    }

    public void setListContent(UserContentResult videoResult) {

        CustomListRowPresenter customListRowPresenter = new CustomListRowPresenter(FocusHighlight.ZOOM_FACTOR_LARGE, false);
        customListRowPresenter.setShadowEnabled(false);
        mCategoryRowAdapter = new ArrayObjectAdapter(customListRowPresenter);
        cardPresenter = new CardPresenter();

        List<VideoItem> watchList = videoResult.getWishlist();
        List<VideoItem> favoritenList = videoResult.getFavoriten();

        if(watchList != null && watchList.size() > 0 ){
            HeaderItem cardPresenterHeader = new HeaderItem(0, "WatchList");
            ArrayObjectAdapter cardRowAdapter ;
            cardRowAdapter = new ArrayObjectAdapter(cardPresenter);
            cardRowAdapter.addAll(0, watchList);
            mCategoryRowAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
        }
        if(favoritenList != null && favoritenList.size() > 0 ){
            HeaderItem cardPresenterHeader = new HeaderItem(1, "Favoriten");
            ArrayObjectAdapter cardRowAdapter ;
            cardRowAdapter = new ArrayObjectAdapter(cardPresenter);
            cardRowAdapter.addAll(0, favoritenList);
            mCategoryRowAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
        }

        setAdapter(mCategoryRowAdapter);

    }


    private class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder viewHolder, Object item, RowPresenter.ViewHolder viewHolder1, Row row) {


            if (item instanceof VideoItem) {
                VideoItem categoryItem = (VideoItem) item;
                requireActivity().startActivity(new Intent(getActivity(), DetailActivity.class).putExtra("slug",categoryItem.getSlug()));
            }
        }
    }

    protected class SpiritItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder viewHolder, Object item, RowPresenter.ViewHolder viewHolder1, Row row) {


            if (item instanceof VideoItem) {
                VideoItem categoryItem = (VideoItem) item;
                requireActivity().startActivity(new Intent(getActivity(), SpiritDetailActivity.class).putExtra("slug",categoryItem.getSlug()));
            }
        }
    }


    HomeFragmentRowSelectedListener homeFragmentSelectedCallBack;

    public void setFavouriteItemViewSelectedListener( HomeFragmentRowSelectedListener callback ) {
        this.homeFragmentSelectedCallBack = callback;
    }

    public interface HomeFragmentRowSelectedListener {
          void onSelected(VideoItem content);
    }


    private class ItemViewSelectedListener implements OnItemViewSelectedListener {
        @SuppressLint("ResourceType")
        @Override
        public void onItemSelected(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder1, Row row) {
            if (o instanceof VideoItem) {
                VideoItem categoryItem = (VideoItem) o;
                if(homeFragmentSelectedCallBack != null){
                    homeFragmentSelectedCallBack.onSelected(categoryItem);
                }

            }
        }
    }

    public void showDialog() {
        int mStackLevel = 5;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
        Fragment prev = context.getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        policyDialogFragment = PrivacyNotifyFragment.newInstance(mStackLevel);
        policyDialogFragment.show(ft, "dialog");
    }

    public void dismissNotifyDialog(){
        policyDialogFragment.dismiss();
    }
}

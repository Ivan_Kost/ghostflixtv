package io.ghostflix.tv.fragmentui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.content.ContentResult;
import io.ghostflix.tv.model.content.VideoItem;
import io.ghostflix.tv.model.episode.EpisodeResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.utils.SessionManager;
import com.bumptech.glide.Glide;

import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeFragment  extends Fragment implements SwimlaneFragment.HomeFragmentRowSelectedListener {


    protected SwimlaneFragment mSwimlaneFragment;

    protected TextView mTextViewTittle;
    protected TextView mTextViewDesc;
    protected TextView rating;
    protected ImageView mImageView;
    protected SessionManager mSessionManager;
    protected Retrofit apiClient;
    protected List<ContentResult> contentResultList;
    protected ContentResult top10ResultList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSwimlaneFragment = new SwimlaneFragment();
        mSwimlaneFragment.setupEventListeners();
        mSessionManager = new SessionManager(getActivity());
         apiClient = ApiClient.getClient();
         if (mSwimlaneFragment != null) {
            mSwimlaneFragment.setFavouriteItemViewSelectedListener(this);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home_fragment, container, false);
        mTextViewTittle = view.findViewById(R.id.title);
        mTextViewDesc = view.findViewById(R.id.description);
        mImageView = view.findViewById(R.id.background_image);
        rating = view.findViewById(R.id.rating);
        replaceFragment();
        return view;
    }

    protected void replaceFragment() {
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.home_browse_fragment, mSwimlaneFragment, "SwimlaneFragment");
        ft.commit();
    }

    public void setListContent(List<ContentResult> contentResultList) {
        this.contentResultList = contentResultList;
        if(this.top10ResultList != null){
            this.contentResultList.add(0,this.top10ResultList);
            mSwimlaneFragment.setSwimLaneContent(contentResultList);
        }

    }

    public void setTopListContent(ContentResult topResultList) {
        this.top10ResultList = topResultList;
        if(this.contentResultList != null){
            this.contentResultList.add(0,this.top10ResultList);
            mSwimlaneFragment.setSwimLaneContent(contentResultList);
        }


    }

    @Override
    public void onSelected(VideoItem content) {
        mTextViewTittle.setText(content.getTitle());
        mTextViewDesc.setText(content.getExcerpt());
        //getEpisodeList(content.getSlug());
        Glide.with(getContext())
                .load(content.getImage())
                .into(mImageView);

    }


    private void getEpisodeList(String slug) {

        Call<EpisodeResult> service = apiClient.create(Service.class).getSeriesDetail(slug, mSessionManager.getToken());

        service.enqueue(new Callback<EpisodeResult>() {
            @Override
            public void onResponse(Call<EpisodeResult> call, Response<EpisodeResult> response) {

                if (response.body() != null) {
                    Glide.with(getContext())
                            .load(response.body().getHeader_background())
                            .into(mImageView);
                }
            }

            @Override
            public void onFailure(Call<EpisodeResult> call, Throwable t) {

            }
        });

    }
}

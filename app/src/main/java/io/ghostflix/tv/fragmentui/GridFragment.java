package io.ghostflix.tv.fragmentui;

import android.os.Bundle;

import androidx.leanback.app.VerticalGridSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;

import io.ghostflix.tv.model.content.VideoItem;
import io.ghostflix.tv.presenter.CardPresenter;
import io.ghostflix.tv.presenter.CustomVerticalGridPresenter;

import java.util.List;


public class GridFragment extends VerticalGridSupportFragment {

    private static final int NUM_COLUMNS = 6;
    private ArrayObjectAdapter mAdapter;
    public  int mLastSelectedItem = 0;
    public CustomVerticalGridPresenter gridPresenter;
    private String CatID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         gridPresenter = new CustomVerticalGridPresenter();
         gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

        setOnItemViewSelectedListener(new OnItemViewSelectedListener() {
            @Override
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {
                mLastSelectedItem = mAdapter.indexOf(item);
            }
        });

        setOnItemViewClickedListener(new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {



            }
        });
    }

    public void setupFragment(List<VideoItem> videoPlayLists) {

        mAdapter = new ArrayObjectAdapter(new CardPresenter());

        for (int i = 0; i < videoPlayLists.size(); i++) {
            mAdapter.add(videoPlayLists.get(i));
        }
        setAdapter(mAdapter);

    }
}

package io.ghostflix.tv.fragmentui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.leanback.app.VerticalGridSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.FocusHighlight;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.content.ContentResult;
import io.ghostflix.tv.model.content.VideoItem;
import io.ghostflix.tv.presenter.CardPresenter;
import io.ghostflix.tv.presenter.CustomListRowPresenter;
import io.ghostflix.tv.presenter.CustomVerticalGridPresenter;
import io.ghostflix.tv.ui.DetailActivity;

import java.util.Objects;


public class SearchGridFragment extends VerticalGridSupportFragment {

    private static final int NUM_COLUMNS = 6;
    private ArrayObjectAdapter mAdapter;
    public  int mLastSelectedItem = 0;
    public CustomVerticalGridPresenter gridPresenter;
    private String CatID;

    private CardPresenter cardPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         gridPresenter = new CustomVerticalGridPresenter();
         gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

        setOnItemViewSelectedListener(new SItemViewSelectedListener());
        setOnItemViewClickedListener(new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {
                if (item instanceof VideoItem) {
                    VideoItem categoryItem = (VideoItem) item;
                    Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), DetailActivity.class).putExtra("slug",categoryItem.getSlug()));
                }

            }
        });
    }

    public void setupFragment(ContentResult videoResult) {

       CardPresenter cardPresenter = new CardPresenter();
        mAdapter = new ArrayObjectAdapter(cardPresenter);
        mAdapter.addAll(0, videoResult.getItems());
        setAdapter(mAdapter);

    }

    SearchGridFragment.SearchFragmentRowSelectedListener searchFragmentSelectedCallBack;

    public void setFavouriteItemViewSelectedListener( SearchGridFragment.SearchFragmentRowSelectedListener callback ) {
        this.searchFragmentSelectedCallBack = callback;
    }

    public interface SearchFragmentRowSelectedListener {
        void onSelected(VideoItem content);
    }

    private class SItemViewSelectedListener implements OnItemViewSelectedListener {
        @SuppressLint("ResourceType")
        @Override
        public void onItemSelected(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder1, Row row) {
            if (o instanceof VideoItem) {
                VideoItem categoryItem = (VideoItem) o;
                if(searchFragmentSelectedCallBack != null){
                    searchFragmentSelectedCallBack.onSelected(categoryItem);
                }

                mLastSelectedItem = mAdapter.indexOf(o);
            }else{
                Log.d("TAG", "onItemSelected2: ");
            }
        }
    }

}

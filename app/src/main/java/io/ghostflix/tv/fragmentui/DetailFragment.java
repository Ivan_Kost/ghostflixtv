package io.ghostflix.tv.fragmentui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.leanback.app.RowsSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.BaseOnItemViewClickedListener;
import androidx.leanback.widget.BaseOnItemViewSelectedListener;
import androidx.leanback.widget.FocusHighlight;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.RowPresenter;

import java.util.List;
import java.util.Map;

import io.ghostflix.tv.media.ExoPlayerActivity;
import io.ghostflix.tv.model.content.AWSUrls;
import io.ghostflix.tv.model.episode.EpisodeContent;
import io.ghostflix.tv.model.video.SignedUrlsRequest;
import io.ghostflix.tv.model.video.SignedUrlsResult;
import io.ghostflix.tv.model.video.TextTracksApiResult;
import io.ghostflix.tv.model.video.VideoApiResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.presenter.CustomListRowPresenter;
import io.ghostflix.tv.presenter.EpisodeCardPresenter;
import io.ghostflix.tv.presenter.SpiritEpisodeCardPresenter;
import io.ghostflix.tv.ui.BaseActivity;
import io.ghostflix.tv.utils.PictureUtils;
import io.ghostflix.tv.utils.PlayerHelper;
import io.ghostflix.tv.utils.PlayerUtil;
import io.ghostflix.tv.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailFragment extends RowsSupportFragment {
    private ArrayObjectAdapter mCategoryRowAdapter;
    private String mCurrentSlug;
    private long mResumeTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpUiElements();
        setUpEventListners();
    }

    private void setUpEventListners() {
        setOnItemViewClickedListener(new ItemViewClickedListner());
        setOnItemViewSelectedListener(new BaseOnItemViewSelectedListener() {
            @Override
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Object row) {

                if (item instanceof EpisodeContent) {
                    EpisodeContent episodeContent = (EpisodeContent) item;
                    mResumeTime = 0;
                    if ( !episodeContent.isIs_watched() && episodeContent.getTime() != null && episodeContent.getTime() > 0) {
                        try {
                            long time = episodeContent.getTime();
                            mResumeTime = time;
                        } catch (NumberFormatException e){
                            mResumeTime = 0;
                        }
                    }
                    detailFragmentItemSelectedListener.onSelected(episodeContent);
                }
            }
        });
    }

    private void setUpUiElements() {

    }


    DetailFragmentItemSelectedListener detailFragmentItemSelectedListener;

    public void setFavouriteItemViewSelectedListener(DetailFragmentItemSelectedListener callback) {
        this.detailFragmentItemSelectedListener = callback;
    }

    public interface DetailFragmentItemSelectedListener {
        void onSelected(EpisodeContent categoryItem);
    }

    public void displayBottomList(String slug, Map<String, List<EpisodeContent>> episodes) {

        int i = 0;
        mCurrentSlug = slug;
        CustomListRowPresenter customListRowPresenter = new CustomListRowPresenter(FocusHighlight.ZOOM_FACTOR_NONE, false);
        customListRowPresenter.setShadowEnabled(false);
        mCategoryRowAdapter = new ArrayObjectAdapter(customListRowPresenter);
        EpisodeCardPresenter cardPresenter = new EpisodeCardPresenter();
        for (Map.Entry<String, List<EpisodeContent>> entry : episodes.entrySet()) {
            Log.d("HERE ", "displayBottomList: " + entry.getKey());
            HeaderItem cardPresenterHeader = new HeaderItem(i, entry.getKey());
            ArrayObjectAdapter cardRowAdapter = new ArrayObjectAdapter(cardPresenter);

            if (entry.getValue() != null && entry.getValue().size() > 0) {
                cardRowAdapter.addAll(0, entry.getValue());
                Log.d("HERE ", "displayBottomList: " + entry.getValue());
                mCategoryRowAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
            }
            break;
        }
        setAdapter(mCategoryRowAdapter);

    }

    public void displaySpiritBottomList(String slug, Map<String, List<EpisodeContent>> episodes) {

        int i = 0;
        mCurrentSlug = slug;
        CustomListRowPresenter customListRowPresenter = new CustomListRowPresenter(FocusHighlight.ZOOM_FACTOR_NONE, false);
        customListRowPresenter.setShadowEnabled(false);
        customListRowPresenter.setSpiritColors();
        mCategoryRowAdapter = new ArrayObjectAdapter(customListRowPresenter);
        SpiritEpisodeCardPresenter cardPresenter = new SpiritEpisodeCardPresenter();
        for (Map.Entry<String, List<EpisodeContent>> entry : episodes.entrySet()) {
            Log.d("HERE ", "displaySpiritBottomList: " + entry.getKey());
            HeaderItem cardPresenterHeader = new HeaderItem(i, entry.getKey());
            ArrayObjectAdapter cardRowAdapter = new ArrayObjectAdapter(cardPresenter);

            if (entry.getValue() != null && entry.getValue().size() > 0) {
                cardRowAdapter.addAll(0, entry.getValue());
                Log.d("HERE ", "displaySpiritBottomList: " + entry.getValue());
                mCategoryRowAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
            }
            break;
        }
        setAdapter(mCategoryRowAdapter);

    }
        private class ItemViewClickedListner implements BaseOnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Object row) {

            if (item instanceof EpisodeContent) {
                Retrofit apiClient = ApiClient.getClient();
                EpisodeContent episodeContent = (EpisodeContent) item;
                Log.d("episodeContent", "onItemClicked: " + episodeContent);
                Log.d("episodeContent", "onItemClicked: " + episodeContent.getVimeoVideo());
                Log.d("MSG", "debug: check if Episode content is locked!");

                if(episodeContent.isLocked()){
                    Log.d("MSG", "debug: Episode content is locked!");
                    return;
                }
                String mp4UrlSource = episodeContent.getMp4_video();
                String hlsUrlSource = episodeContent.getHls_video();
                if(hlsUrlSource != null && !hlsUrlSource.equals("")) {
                    Intent intent = PlayerHelper.createHLSVideoIntent(episodeContent, (BaseActivity) getActivity(), mCurrentSlug,hlsUrlSource,episodeContent.getTextrack());
                    intent.putExtra("resume",mResumeTime);
                    startActivity(intent);
                }else if (mp4UrlSource != null && !mp4UrlSource.equals("")) {
                    SessionManager sessionManager = new SessionManager(getContext());
                    AWSUrls awsUrls = new AWSUrls();
                    awsUrls.setSdVideo(episodeContent.getMp4_video());
                    awsUrls.setHdVideo(episodeContent.getMp4_video_hd());
                    awsUrls.setTextTrack(episodeContent.getTextrack());
                    SignedUrlsRequest signedUrlsRequest = new SignedUrlsRequest();
                    signedUrlsRequest.setAws_url(awsUrls);
                    Call<SignedUrlsResult> service = apiClient.create(Service.class).getAwsUrls(signedUrlsRequest,sessionManager.getToken());
                    service.enqueue(new Callback<SignedUrlsResult>() {
                        @Override
                        public void onResponse(Call<SignedUrlsResult> call, Response<SignedUrlsResult> response) {
                            if (response != null && response.body() != null) {
                                SignedUrlsResult signedUrlsResult = response.body();
                                Intent intent = PlayerHelper.createVideoIntent(episodeContent, (BaseActivity) getActivity(), mCurrentSlug,signedUrlsResult.getSigned_url());
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.putExtra("resume",mResumeTime);
                                Log.d("IDebug","Start aws activity");
                                startActivity(intent);
                            }
                        }
                        @Override
                        public void onFailure(Call<SignedUrlsResult> call, Throwable t) {
                            Log.d("MSG", "onFailure: " + t.getLocalizedMessage());
                            Log.d("MSG", "onFailure: " + t.getMessage());
                            Log.d("MSG", "onFailure: " + t.getCause());
                        }
                    });
                } else {
                    // ""  "427184fae1c124e6a897b87c6b9a41a0"
                   // String vimeoId = "https://player.vimeo.com/video/" + episodeContent.getVimeoVideo() + "/config";
                    String apiUrl = "https://api.vimeo.com/videos/"+ episodeContent.getVimeoVideo();
                    String textTrackApiUrl = apiUrl + "/texttracks";
                    String header = "Bearer fd9ba317a092fde3ded0c9d6f04fa89b";
                    Intent intent = new Intent(getActivity(), ExoPlayerActivity.class);
                    intent.setAction(Intent.ACTION_VIEW);
                    final boolean[] videoIsReady = {false};
                    final boolean[] textTrackIsReady = {false};
                    Call<VideoApiResult> service = apiClient.create(Service.class).getVideoApiResult(apiUrl, header);
                    Call<TextTracksApiResult> textTrackService = apiClient.create(Service.class).getApiSubtitlesResult(textTrackApiUrl,header);
                    service.enqueue(new Callback<VideoApiResult>() {
                        @Override
                        public void onResponse(Call<VideoApiResult> call, Response<VideoApiResult> response) {

                            if (response != null && response.body() != null) {
                                VideoApiResult channelResponse = response.body();
                               // Intent intent = new Intent(getActivity(), ExoPlayerActivity.class);
                                intent.putExtra("VideoTitle", episodeContent.getEpisodeTitle());
                                intent.putExtra("VidDesc", episodeContent.getDescription());
                                intent.putExtra("VideoLink", PictureUtils.getApiVideo(channelResponse.getFiles()));
                                intent.putExtra("videoLinksArray",PictureUtils.getVimeoArray(channelResponse.getFiles()));
                                intent.putExtra(PlayerUtil.MIME_TYPE_EXTRA,"video/mp4");
                                intent.putExtra(PlayerUtil.TITLE_EXTRA,episodeContent.getEpisodeTitle());
                               // intent.putExtra("VideoLink", episodeContent.getMp4_video());
                               // intent.putExtra("subTitleLink", PictureUtils.getSubtitle(channelResponse.getRequest().getTextTracksList()));
                                if (episodeContent.getForceSubtitles() != null && episodeContent.getForceSubtitles().equalsIgnoreCase("on")) {
                                    intent.putExtra("force_subtitle", true);
                                }
                             //   intent.putExtra("vimeoId", vimeoId);
                                intent.putExtra("slug", mCurrentSlug);
                                intent.putExtra("video_id", episodeContent.getVideo_id());
                                intent.putExtra("skipIntroTime",episodeContent.getSkipIntroTime());
                                intent.putExtra("resume",mResumeTime);
                                videoIsReady[0] = true;
                                if (textTrackIsReady[0]){
                                    Log.d("IDebug","Text track is ready. Start Activity");
                                    startActivity(intent);
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<VideoApiResult> call, Throwable t) {
                            Log.d("MSG", "onFailure: " + t.getLocalizedMessage());
                            Log.d("MSG", "onFailure: " + t.getMessage());
                            Log.d("MSG", "onFailure: " + t.getCause());
                        }
                    });

                    textTrackService.enqueue(new Callback<TextTracksApiResult>() {
                        @Override
                        public void onResponse(Call<TextTracksApiResult> call, Response<TextTracksApiResult> response) {

                            if (response != null && response.body() != null) {
                                TextTracksApiResult channelResponse = response.body();
                                 intent.putExtra("subTitleLink", PictureUtils.getTextTrack(channelResponse.getTextTracks()));
                                intent.putExtra(PlayerUtil.SUBTITLE_URI_EXTRA,PictureUtils.getTextTrack(channelResponse.getTextTracks()));

                                if (episodeContent.getForceSubtitles() != null && episodeContent.getForceSubtitles().equalsIgnoreCase("on")) {
                                    intent.putExtra("force_subtitle", true);
                                }
                                //   intent.putExtra("vimeoId", vimeoId);
                                textTrackIsReady[0] = true;
                                if (videoIsReady[0]){
                                    startActivity(intent);
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<TextTracksApiResult> call, Throwable t) {

                            Log.d("MSG", "onFailure: " + t.getLocalizedMessage());
                            Log.d("MSG", "onFailure: " + t.getMessage());
                            Log.d("MSG", "onFailure: " + t.getCause());
                        }
                    });
                }
            }
        }
    }
}
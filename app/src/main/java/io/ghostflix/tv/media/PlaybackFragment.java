package io.ghostflix.tv.media;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.accessibility.CaptioningManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.leanback.app.VideoSupportFragment;
import androidx.leanback.app.VideoSupportFragmentGlueHost;
import androidx.leanback.media.PlaybackGlue;
import androidx.leanback.widget.PlaybackSeekDataProvider;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Tracks;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.ext.leanback.LeanbackPlayerAdapter;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelectionParameters;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.DebugTextViewHelper;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.ghostflix.tv.R;
import io.ghostflix.tv.dialog.TrackSelectionDialog;
import io.ghostflix.tv.model.user.TokenResult;
import io.ghostflix.tv.model.user.VideoParams;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.utils.DemoUtil;
import io.ghostflix.tv.utils.DownloadTracker;
import io.ghostflix.tv.utils.PlayerUtil;
import io.ghostflix.tv.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PlaybackFragment extends VideoSupportFragment implements View.OnClickListener {

    private static final int UPDATE_DELAY = 16;

    private VideoPlayerGlue mPlayerGlue;
    private LeanbackPlayerAdapter mPlayerAdapter;
    private ExoPlayer mPlayer;
    private TrackSelector mTrackSelector;

    private String VidTitle;
    private String VidUrl;
    private String VidDesc;
    private String subTitleLink;
    private SubtitleView subtitles;
    private boolean mIsCaptionOn = false;
    private SessionManager sessionManager;
    private String vimeoId;
    private String mCurrentSlug;
    private String video_id;
    private long resumeTime;
    private final String TAG = PlaybackFragment.class.getName();
    private Runnable runnable;
    private boolean isForceSubtitle;
    private int introTimeSec;
    private Button skipIntroButton;

    private List<MediaItem> mediaItems;
    private TrackSelectionParameters trackSelectionParameters;
    private static final String KEY_TRACK_SELECTION_PARAMETERS = "track_selection_parameters";
    private static final String KEY_SERVER_SIDE_ADS_LOADER_STATE = "server_side_ads_loader_state";
    private static final String KEY_ITEM_INDEX = "item_index";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private boolean startAutoPlay;
    private int startItemIndex;
    private long startPosition;
    protected StyledPlayerView playerView;
    protected LinearLayout debugRootView;
    protected TextView debugTextView;
    private Button selectTracksButton;
    private boolean isShowingTrackSelectionDialog;
    private DebugTextViewHelper debugViewHelper;
    private Tracks lastSeenTracks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VidTitle = getActivity().getIntent().getStringExtra("VideoTitle");
        VidUrl = getActivity().getIntent().getStringExtra("VideoLink");
        VidDesc = getActivity().getIntent().getStringExtra("VidDesc");
        subTitleLink = getActivity().getIntent().getStringExtra("subTitleLink");
        vimeoId = getActivity().getIntent().getStringExtra("vimeoId");
        mCurrentSlug = getActivity().getIntent().getStringExtra("slug");
        video_id = getActivity().getIntent().getStringExtra("video_id");
        resumeTime = getActivity().getIntent().getLongExtra("resume", 0);
        isForceSubtitle = getActivity().getIntent().getBooleanExtra("force_subtitle", false);
        String timeStr = getActivity().getIntent().getStringExtra("skipIntroTime");
        introTimeSec =  timeStr == null || timeStr.equals("") ? 0 : Integer.parseInt(timeStr);
        sessionManager = new SessionManager(getActivity());
        if(subTitleLink!= null && isForceSubtitle){
            mIsCaptionOn = true;
        }
        Log.d("IDebug", "Before new handler" );
       Handler handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (mPlayer != null) {
                //    setTime(mPlayer.getContentPosition(), mPlayer.getDuration());

                }
                handler.postDelayed(runnable, 10000);
            }
        };
        handler.postDelayed(runnable, 0);

        if (savedInstanceState != null) {
            Log.d("IDebug", "In saved instance State" );
            trackSelectionParameters =
                    TrackSelectionParameters.fromBundle(
                            savedInstanceState.getBundle(KEY_TRACK_SELECTION_PARAMETERS));
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startItemIndex = savedInstanceState.getInt(KEY_ITEM_INDEX);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            Log.d("IDebug", "Instance state isn't saved" );
            trackSelectionParameters = new TrackSelectionParameters.Builder(/* context= */ this.getContext()).build();
            clearStartPosition();
        }

       // debugRootView = getActivity().findViewById(R.id.controls_root);
      //  debugTextView = getActivity().findViewById(R.id.debug_text_view);
        selectTracksButton = getActivity(). findViewById(R.id.exo_hd_sd);
        selectTracksButton.setOnClickListener(this);
        Log.d("IDebug", "end OnCreate" );
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || mPlayer == null)) {
            initializePlayer();
        }
    }

    /** Pauses the player. */
    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onPause() {
        super.onPause();

        if (mPlayerGlue != null && mPlayerGlue.isPlaying()) {
            mPlayerGlue.pause();
        }
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
      /*  if (mPlayer != null) {
            DetailActivity da = (DetailActivity)Objects.requireNonNull(getActivity().getParent());
            da.mCurrentEpisodeContent.setTime(mPlayer.getCurrentPosition());
        }*/
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private boolean initializePlayer() {
        Log.d("IDebug", "Start initialize player" );
        BandwidthMeter bandwidthMeter =  DefaultBandwidthMeter.getSingletonInstance(getContext());
        if (mPlayer == null) {
            Log.d("IDebug", "Create Exo Player" );
            mPlayer =  new ExoPlayer.Builder(getContext()).build();
            subtitles= getActivity().findViewById(R.id.leanback_subtitles);
            Intent intent = getActivity().getIntent();

            mPlayer.setTrackSelectionParameters(trackSelectionParameters);
            mPlayer.addListener(new PlayerEventListener());
            mediaItems = createMediaItems(intent);
            mPlayer.addAnalyticsListener(new EventLogger());
            mPlayer.setAudioAttributes(AudioAttributes.DEFAULT, /* handleAudioFocus= */ true);
            mPlayer.setPlayWhenReady(startAutoPlay);
            playerView.setPlayer(mPlayer);
           // configurePlayerWithServerSideAdsLoader();
           // debugViewHelper = new DebugTextViewHelper(mPlayer, debugTextView);
            debugViewHelper.start();
            Log.d("IDebug", "Exo player is created" );
        }
        boolean haveStartPosition = startItemIndex != C.INDEX_UNSET;
        if (haveStartPosition) {
            Log.d("IDebug", "Seek to start position" );
            mPlayer.seekTo(startItemIndex, startPosition);
        }
        mPlayer.setMediaItems(mediaItems, /* resetPosition= */ !haveStartPosition);
        mPlayer.prepare();

        updateButtonVisibility();
        return true;


       /* if(!isForceSubtitle){
            subtitles.setVisibility(View.GONE);
        }
        mPlayerAdapter = new LeanbackPlayerAdapter(getActivity(), mPlayer, UPDATE_DELAY);
        mPlayerGlue = new VideoPlayerGlue(getActivity(), mPlayerAdapter, null,subtitles, mIsCaptionOn);
        mPlayerGlue.setHost(new VideoSupportFragmentGlueHost(this));

        mPlayerGlue.addPlayerCallback(new PlaybackGlue.PlayerCallback() {
            @Override
            public void onPreparedStateChanged(PlaybackGlue glue) {
                if (glue.isPrepared()) {
                    mPlayerGlue.setSeekProvider(new PlaybackSeekDataProvider());
                    mPlayerGlue.play();
                }
            }
        });

        mPlayerGlue.setControlsOverlayAutoHideEnabled(true);
        mPlayerGlue.playWhenPrepared();

        skipIntroButton = getActivity().findViewById(R.id.skip_intro);
        skipIntroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skipIntro(view);
            }
        });

        mPlayer.setMediaItems(mediaItems, *//* resetPosition= *//* !haveStartPosition);
        play();*/
    }



    private void releasePlayer() {
        if (mPlayer != null) {
            setTime(mPlayer.getCurrentPosition(), mPlayer.getDuration());
            mPlayer.release();
            mPlayer = null;
            mTrackSelector = null;
            mPlayerGlue = null;
            mPlayerAdapter = null;
        }
    }

    private void play() {
        initializePlayer();
        /*if(VidUrl != null && VidUrl.length() > 0){
            mPlayerGlue.setTitle(VidTitle);
            mPlayerGlue.setSubtitle(VidDesc);
            prepareMediaForPlaying(Uri.parse(VidUrl));
            mPlayerGlue.play();
            Log.d("*******", "play: ");
            if(resumeTime > 0){
                mPlayer.seekTo(resumeTime );
            }


        }*/

    }


    private boolean linkIsExist(String url){
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con =  (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("HEAD");
            System.out.println(con.getResponseCode());
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



    private static List<MediaItem> createMediaItems(Intent intent, DownloadTracker downloadTracker) {
        List<MediaItem> mediaItems = new ArrayList<>();
        for (MediaItem item : PlayerUtil.createMediaItemsFromIntent(intent, true)) {
            mediaItems.add(
                    maybeSetDownloadProperties(
                            item, downloadTracker.getDownloadRequest(item.localConfiguration.uri)));
        }
        return mediaItems;
    }

    public void skipIntro(View view) {
        Log.d(TAG, "Skip intro to " + introTimeSec);
        mPlayerGlue.seekTo((introTimeSec* 1000L));
        skipIntroButton.setVisibility(View.GONE);
    }


    public void skipToNext() {
        //mPlayerGlue.next();
    }

    public void skipToPrevious() {
        //mPlayerGlue.previous();
    }

    public void rewind() {
        mPlayerGlue.rewind();
    }

    public void fastForward() {
        mPlayerGlue.fastForward();
    }

    public  boolean isCaptionsEnabled(Context context) {
        CaptioningManager captioningManager = (CaptioningManager) context.getSystemService(Context.CAPTIONING_SERVICE);
        if (null != captioningManager) {
            return captioningManager.isEnabled();
        }
        return false;
    }

    private void setTime(long contentPosition, long duration){

        Retrofit apiClient = ApiClient.getClient();
        SessionManager sessionManager = new SessionManager(getContext());
        VideoParams requestParam = new VideoParams();
        requestParam.setDuration(duration/1000);
        requestParam.setVideo_id(video_id);
        requestParam.setTime(contentPosition/1000);
        requestParam.setSlug(mCurrentSlug);
        Call<TokenResult> service = apiClient.create(Service.class).setTime(mCurrentSlug,requestParam, sessionManager.getToken());
        service.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {
                Log.d(TAG,response.message());

            }
            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {
                Log.d(TAG,t.getMessage());

            }
        });


    }

    protected void clearStartPosition() {
        startAutoPlay = true;
        startItemIndex = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    private void updateButtonVisibility() {
        Log.d("IDebug", "Update button visibility" );
        selectTracksButton.setEnabled(mPlayer != null && TrackSelectionDialog.willHaveContent(mPlayer));
    }

    private void showControls() {
        debugRootView.setVisibility(View.VISIBLE);
    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }


    private class PlayerEventListener implements Player.Listener {

        @Override
        public void onPlaybackStateChanged(@Player.State int playbackState) {
            if (playbackState == Player.STATE_ENDED) {
                showControls();
            }
            updateButtonVisibility();
        }

        @Override
        public void onPlayerError(PlaybackException error) {
            if (error.errorCode == PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW) {
                mPlayer.seekToDefaultPosition();
                mPlayer.prepare();
            } else {
                updateButtonVisibility();
                showControls();
            }
        }

        @Override
        @SuppressWarnings("ReferenceEquality")
        public void onTracksChanged(Tracks tracks) {
            updateButtonVisibility();
            if (tracks == lastSeenTracks) {
                return;
            }
            if (tracks.containsType(C.TRACK_TYPE_VIDEO)
                    && !tracks.isTypeSupported(C.TRACK_TYPE_VIDEO, /* allowExceedsCapabilities= */ true)) {
                showToast(R.string.error_unsupported_video);
            }
            if (tracks.containsType(C.TRACK_TYPE_AUDIO)
                    && !tracks.isTypeSupported(C.TRACK_TYPE_AUDIO, /* allowExceedsCapabilities= */ true)) {
                showToast(R.string.error_unsupported_audio);
            }
            lastSeenTracks = tracks;
        }

    }


    private List<MediaItem> createMediaItems(Intent intent) {
        String action = intent.getAction();
        boolean actionIsListView = PlayerUtil.ACTION_VIEW_LIST.equals(action);
        if (!actionIsListView && !PlayerUtil.ACTION_VIEW.equals(action)) {
            showToast(getString(R.string.unexpected_intent_action, action));
            getActivity().finish();
            return Collections.emptyList();
        }

        List<MediaItem> mediaItems =
                createMediaItems(intent, DemoUtil.getDownloadTracker(/* context= */ getActivity()));
        for (int i = 0; i < mediaItems.size(); i++) {
            MediaItem mediaItem = mediaItems.get(i);

            if (!Util.checkCleartextTrafficPermitted(mediaItem)) {
                showToast(R.string.error_cleartext_not_permitted);
                getActivity().finish();
                return Collections.emptyList();
            }
            if (Util.maybeRequestReadExternalStoragePermission(/* activity= */ getActivity(), mediaItem)) {
                // The player will be reinitialized if the permission is granted.
                return Collections.emptyList();
            }

            MediaItem.DrmConfiguration drmConfiguration = mediaItem.localConfiguration.drmConfiguration;
            if (drmConfiguration != null) {
                if (Build.VERSION.SDK_INT < 18) {
                    showToast(R.string.error_drm_unsupported_before_api_18);
                    getActivity().finish();
                    return Collections.emptyList();
                } else if (!FrameworkMediaDrm.isCryptoSchemeSupported(drmConfiguration.scheme)) {
                    showToast(R.string.error_drm_unsupported_scheme);
                    getActivity().finish();
                    return Collections.emptyList();
                }
            }
        }
        return mediaItems;
    }


    @Override
    public void onClick(View view) {
        if (view == selectTracksButton
                && !isShowingTrackSelectionDialog
                && TrackSelectionDialog.willHaveContent(mPlayer)) {
            isShowingTrackSelectionDialog = true;
            TrackSelectionDialog trackSelectionDialog =
                    TrackSelectionDialog.createForPlayer(
                            mPlayer,
                            /* onDismissListener= */ dismissedDialog -> isShowingTrackSelectionDialog = false);
            trackSelectionDialog.show(getActivity().getSupportFragmentManager(), /* tag= */ null);
        }
    }

    private static MediaItem maybeSetDownloadProperties(
            MediaItem item, @Nullable DownloadRequest downloadRequest) {
        if (downloadRequest == null) {
            return item;
        }
        MediaItem.Builder builder = item.buildUpon();
        builder
                .setMediaId(downloadRequest.id)
                .setUri(downloadRequest.uri)
                .setCustomCacheKey(downloadRequest.customCacheKey)
                .setMimeType(downloadRequest.mimeType)
                .setStreamKeys(downloadRequest.streamKeys);
        @Nullable
        MediaItem.DrmConfiguration drmConfiguration = item.localConfiguration.drmConfiguration;
        if (drmConfiguration != null) {
            builder.setDrmConfiguration(
                    drmConfiguration.buildUpon().setKeySetId(downloadRequest.keySetId).build());
        }
        return builder.build();
    }
}
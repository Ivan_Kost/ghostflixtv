package io.ghostflix.tv.media;

import android.content.Context;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.leanback.media.PlaybackTransportControlGlue;
import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.PlaybackControlsRow;

import io.ghostflix.tv.R;
import com.google.android.exoplayer2.ext.leanback.LeanbackPlayerAdapter;
import com.google.android.exoplayer2.ui.SubtitleView;

import java.util.concurrent.TimeUnit;

/**
 * Manages customizing the actions in the {@link PlaybackControlsRow}. Adds and manages the
 * following actions to the primary and secondary controls:
 *
 * <ul>
 *   <li>{@link PlaybackControlsRow.RepeatAction}
 *   <li>{@link PlaybackControlsRow.ThumbsDownAction}
 *   <li>{@link PlaybackControlsRow.ThumbsUpAction}
 *   <li>{@link PlaybackControlsRow.SkipPreviousAction}
 *   <li>{@link PlaybackControlsRow.SkipNextAction}
 *   <li>{@link PlaybackControlsRow.FastForwardAction}
 *   <li>{@link PlaybackControlsRow.RewindAction}
 * </ul>
 *
 * Note that the superclass, {@link PlaybackTransportControlGlue}, manages the playback controls
 * row.
 */
public class VideoPlayerGlue extends PlaybackTransportControlGlue<LeanbackPlayerAdapter> {

    private static final long TEN_SECONDS = TimeUnit.SECONDS.toMillis(10);

    /** Listens for when skip to next and previous actions have been dispatched. */
    public interface OnActionClickedListener {

        /** Skip to the previous item in the queue. */
        void onPrevious();

        /** Skip to the next item in the queue. */
        void onNext();
    }

    private final OnActionClickedListener mActionListener;

    private final PlaybackControlsRow.RepeatAction mRepeatAction;
    private final PlaybackControlsRow.ThumbsUpAction mThumbsUpAction;
    private final PlaybackControlsRow.ThumbsDownAction mThumbsDownAction;
    private final PlaybackControlsRow.SkipPreviousAction mSkipPreviousAction;
    private final PlaybackControlsRow.SkipNextAction mSkipNextAction;
    private final PlaybackControlsRow.FastForwardAction mFastForwardAction;
    private final PlaybackControlsRow.RewindAction mRewindAction;
    private final SubtitleView mSubtitleView;
    private boolean subtitleExist = true;
    private boolean mIsCaptionOn;

    public void hideSubtitle(){
        subtitleExist = false;
    }
    public VideoPlayerGlue(
            Context context,
            LeanbackPlayerAdapter playerAdapter,
            OnActionClickedListener actionListener, SubtitleView subtitles, boolean captionOn) {
        super(context, playerAdapter);

        mActionListener = actionListener;
        mSubtitleView = subtitles;
        mIsCaptionOn = captionOn;
        mSkipPreviousAction = new PlaybackControlsRow.SkipPreviousAction(context);
        mSkipNextAction = new PlaybackControlsRow.SkipNextAction(context);
        mFastForwardAction = new PlaybackControlsRow.FastForwardAction(context);
        mRewindAction = new PlaybackControlsRow.RewindAction(context);

        mThumbsUpAction = new PlaybackControlsRow.ThumbsUpAction(context);
        mThumbsUpAction.setIndex(PlaybackControlsRow.ThumbsUpAction.INDEX_OUTLINE);
        mThumbsDownAction = new PlaybackControlsRow.ThumbsDownAction(context);
        mThumbsDownAction.setIndex(PlaybackControlsRow.ThumbsDownAction.INDEX_OUTLINE);
        mRepeatAction = new PlaybackControlsRow.RepeatAction(context);

        if(mIsCaptionOn){
            mThumbsUpAction.setIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_action_closed_caption));
        }else{
            mThumbsUpAction.setIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_action_closed_caption_off));
        }
    }

    @Override
    protected void onCreatePrimaryActions(ArrayObjectAdapter adapter) {
        // Order matters, super.onCreatePrimaryActions() will create the play / pause action.
        // Will display as follows:
        // play/pause, previous, rewind, fast forward, next
        //   > /||      |<        <<        >>         >|
        super.onCreatePrimaryActions(adapter);
//        adapter.add(mSkipPreviousAction);
        adapter.add(mRewindAction);
        adapter.add(mFastForwardAction);
//        adapter.add(mSkipNextAction);
    }

    @Override
    protected void onCreateSecondaryActions(ArrayObjectAdapter adapter) {
        super.onCreateSecondaryActions(adapter);
        if(subtitleExist){
            adapter.add(mThumbsUpAction);
        }
//        adapter.add(mThumbsDownAction);
//        adapter.add(mThumbsDownAction);
        // adapter.add(mThumbsUpAction);
//        adapter.add(mRepeatAction);
    }

    @Override
    public void onActionClicked(Action action) {
        if (shouldDispatchAction(action)) {
            dispatchAction(action);
            return;
        }
        // Super class handles play/pause and delegates to abstract methods next()/previous().
        super.onActionClicked(action);
    }

    // Should dispatch actions that the super class does not supply callbacks for.
    private boolean shouldDispatchAction(Action action) {
        return action == mRewindAction
                || action == mFastForwardAction
                || action == mThumbsDownAction
                || action == mThumbsUpAction
                || action == mRepeatAction;
    }

    private void dispatchAction(Action action) {
        // Primary actions are handled manually.
        if (action == mRewindAction) {
            rewind();
        } else if (action == mFastForwardAction) {
            fastForward();
        }else if (action == mThumbsUpAction) {
            if(mIsCaptionOn){
                mThumbsUpAction.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_action_closed_caption_off));
                mSubtitleView.setVisibility(View.INVISIBLE);
                mIsCaptionOn = false;
            }else{
                mThumbsUpAction.setIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_action_closed_caption));
                mSubtitleView.setVisibility(View.VISIBLE);
                mIsCaptionOn = true;
            }
            PlaybackControlsRow.MultiAction multiAction = (PlaybackControlsRow.MultiAction) action;
            notifyActionChanged(multiAction,  (ArrayObjectAdapter) getControlsRow().getSecondaryActionsAdapter());
        }

        else if (action instanceof PlaybackControlsRow.MultiAction) {
            PlaybackControlsRow.MultiAction multiAction = (PlaybackControlsRow.MultiAction) action;
            multiAction.nextIndex();
            // Notify adapter of action changes to handle secondary actions, such as, thumbs up/down
            // and repeat.
            notifyActionChanged(
                    multiAction,
                    (ArrayObjectAdapter) getControlsRow().getSecondaryActionsAdapter());
        }
    }

    private void notifyActionChanged(
            PlaybackControlsRow.MultiAction action, ArrayObjectAdapter adapter) {
        if (adapter != null) {
            int index = adapter.indexOf(action);
            if (index >= 0) {
                adapter.notifyArrayItemRangeChanged(index, 1);
            }
        }
    }

    @Override
    public void next() {
        //mActionListener.onNext();
    }

    @Override
    public void previous() {
        //mActionListener.onPrevious();
    }

    /** Skips backwards 10 seconds. */
    public void rewind() {
        long newPosition = getCurrentPosition() - TEN_SECONDS;
        newPosition = (newPosition < 0) ? 0 : newPosition;
        getPlayerAdapter().seekTo(newPosition);
    }

    /** Skips forward 10 seconds. */
    public void fastForward() {
        if (getDuration() > -1) {
            long newPosition = getCurrentPosition() + TEN_SECONDS;
            newPosition = (newPosition > getDuration()) ? getDuration() : newPosition;
            getPlayerAdapter().seekTo(newPosition);
        }
    }
}

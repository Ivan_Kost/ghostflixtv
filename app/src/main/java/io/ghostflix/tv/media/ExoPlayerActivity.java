package io.ghostflix.tv.media;


import static io.ghostflix.tv.utils.PlayerUtil.MIME_TYPE_EXTRA;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.Tracks;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManagerProvider;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.ext.leanback.LeanbackPlayerAdapter;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionParameters;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.google.android.exoplayer2.util.DebugTextViewHelper;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.ghostflix.tv.R;
import io.ghostflix.tv.dialog.TrackSelectionDialog;
import io.ghostflix.tv.model.user.TokenResult;
import io.ghostflix.tv.model.user.VideoParams;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.utils.DemoUtil;
import io.ghostflix.tv.utils.DownloadTracker;
import io.ghostflix.tv.utils.PlayerUtil;
import io.ghostflix.tv.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class ExoPlayerActivity extends FragmentActivity implements View.OnClickListener , StyledPlayerView.ControllerVisibilityListener{

    private static final float GAMEPAD_TRIGGER_INTENSITY_ON = 0.5f;
    // Off-condition slightly smaller for button debouncing.
    private static final float GAMEPAD_TRIGGER_INTENSITY_OFF = 0.45f;
    private static final int UPDATE_DELAY = 16;
    private boolean gamepadTriggerPressed = false;
    private String VidTitle;
    private String VidUrl;
    private String VideoHLSUrl;
    private String[] VideoUrls;
    private String VidDesc;
    private String mimeType;
    private String subTitleLink;
    private SubtitleView subtitles;
    private boolean mIsCaptionOn = false;
    private SessionManager sessionManager;
    private String vimeoId;
    private String mCurrentSlug;
    private String video_id;
    private long resumeTime;
    private final String TAG = PlaybackFragment.class.getName();
    private boolean isForceSubtitle;
    private int introTimeSec;
    private Button skipIntroButton;
    private ExoPlayer mPlayer;
    private List<MediaItem> mediaItems;
    private TrackSelectionParameters trackSelectionParameters;
    private static final String KEY_TRACK_SELECTION_PARAMETERS = "track_selection_parameters";
    private static final String KEY_SERVER_SIDE_ADS_LOADER_STATE = "server_side_ads_loader_state";
    private static final String KEY_ITEM_INDEX = "item_index";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private boolean startAutoPlay;
    private int startItemIndex;
    private long startPosition;
    protected StyledPlayerView playerView;
    protected LinearLayout debugRootView;
    protected TextView debugTextView;
    private ImageButton selectTracksButton;
    private boolean isShowingTrackSelectionDialog;
    private DebugTextViewHelper debugViewHelper;
    private Tracks lastSeenTracks;
    private DataSource.Factory dataSourceFactory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("IDebug","On create Exo player Activity");
        dataSourceFactory = DemoUtil.getDataSourceFactory(/* context= */ this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        VidTitle = this.getIntent().getStringExtra("VideoTitle");
        VidUrl = this.getIntent().getStringExtra("VideoLink");
        mimeType = this.getIntent().getStringExtra(MIME_TYPE_EXTRA);
        VideoHLSUrl = this.getIntent().getStringExtra("VideoHLSLink");
        VideoUrls = this.getIntent().getStringArrayExtra("videoLinksArray");
        VidDesc = this.getIntent().getStringExtra("VidDesc");
        subTitleLink = this.getIntent().getStringExtra("subTitleLink");
        vimeoId = this.getIntent().getStringExtra("vimeoId");
        mCurrentSlug = this.getIntent().getStringExtra("slug");
        video_id = this.getIntent().getStringExtra("video_id");
        resumeTime = this.getIntent().getLongExtra("resume", 0);
        isForceSubtitle = this.getIntent().getBooleanExtra("force_subtitle", false);
        String timeStr = this.getIntent().getStringExtra("skipIntroTime");
        introTimeSec =  timeStr == null || timeStr.equals("") ? 0 : Integer.parseInt(timeStr);
        sessionManager = new SessionManager(this);
        if(subTitleLink!= null && isForceSubtitle){
            mIsCaptionOn = true;
        }
        Log.d("IDebug", "Before new handler" );
        playerView = findViewById(R.id.player_view);
        playerView.setControllerVisibilityListener(this);
        playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
        playerView.requestFocus();

        if (savedInstanceState != null) {
            Log.d("IDebug", "In saved instance State" );
            trackSelectionParameters =
                    TrackSelectionParameters.fromBundle(
                            savedInstanceState.getBundle(KEY_TRACK_SELECTION_PARAMETERS));
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startItemIndex = savedInstanceState.getInt(KEY_ITEM_INDEX);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            Log.d("IDebug", "Instance state isn't saved" );

            trackSelectionParameters = new TrackSelectionParameters.Builder(/* context= */ this).build();
            //clearStartPosition();
        }
        selectTracksButton = this. findViewById(R.id.exo_hd_sd);
        this.findViewById(R.id.exo_prev).setVisibility(View.GONE);
        this.findViewById(R.id.exo_next).setVisibility(View.GONE);
        selectTracksButton.setOnClickListener(this);
        Log.d("IDebug", "end OnCreate" );
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT <= 23) {
            if (playerView != null) {
                playerView.onPause();
            }
            releasePlayer();
        }
    }

    private void updateTrackSelectorParameters() {
        if (mPlayer != null) {
            trackSelectionParameters = mPlayer.getTrackSelectionParameters();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT > 23) {
            if (playerView != null) {
                playerView.onPause();
            }
            releasePlayer();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        outState.putBundle(KEY_TRACK_SELECTION_PARAMETERS, trackSelectionParameters.toBundle());
        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
        outState.putInt(KEY_ITEM_INDEX, startItemIndex);
        outState.putLong(KEY_POSITION, startPosition);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // See whether the player view wants to handle media or DPAD keys events.
        return playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        return super.onKeyDown(keyCode, event);
       /* boolean handled = false;
        if (keyCode == KeyEvent.KEYCODE_BUTTON_R1) {

            handled = true;
        } else if (keyCode == KeyEvent.KEYCODE_BUTTON_L1) {

            handled = true;
        } else if (keyCode == KeyEvent.KEYCODE_BUTTON_L2 ||keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {

            handled = true;
        } else if (keyCode == KeyEvent.KEYCODE_BUTTON_R2 || keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {

            handled = true;
        }

        return handled || super.onKeyDown(keyCode, event);*/
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        // This method will handle gamepad events.
        return super.onGenericMotionEvent(event);
       /* if (event.getAxisValue(MotionEvent.AXIS_LTRIGGER) > GAMEPAD_TRIGGER_INTENSITY_ON
                && !gamepadTriggerPressed) {
            mPlaybackFragment.rewind();
            gamepadTriggerPressed = true;
        } else if (event.getAxisValue(MotionEvent.AXIS_RTRIGGER) > GAMEPAD_TRIGGER_INTENSITY_ON
                && !gamepadTriggerPressed) {
            mPlaybackFragment.fastForward();
            gamepadTriggerPressed = true;
        } else if (event.getAxisValue(MotionEvent.AXIS_LTRIGGER) < GAMEPAD_TRIGGER_INTENSITY_OFF
                && event.getAxisValue(MotionEvent.AXIS_RTRIGGER) < GAMEPAD_TRIGGER_INTENSITY_OFF) {
            gamepadTriggerPressed = false;
        }*/

    }

    @Override
    public void onClick(View view) {
        Log.d("playerOnclickDebug","onclick event");
        if (view == selectTracksButton
                && !isShowingTrackSelectionDialog
                && TrackSelectionDialog.willHaveContent(mPlayer)) {
            Log.d("playerOnclickDebug","show track selection dialog");
            isShowingTrackSelectionDialog = true;
            TrackSelectionDialog trackSelectionDialog =
                    TrackSelectionDialog.createForPlayer(
                            mPlayer,
                            /* onDismissListener= */ dismissedDialog -> isShowingTrackSelectionDialog = false);
            trackSelectionDialog.show(getSupportFragmentManager(), /* tag= */ null);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT > 23) {
            initializePlayer();
            if (playerView != null) {
                playerView.onResume();
            }
        }
    }

    private boolean initializePlayer() {
        Log.d("IDebug", "Start initialize player" );
        if (mPlayer == null) {
            Log.d("IDebug", "Create Exo Player" );
            ExoPlayer.Builder playerBuilder =
                    new ExoPlayer.Builder(/* context= */ this)
                            .setMediaSourceFactory(createMediaSourceFactory());
            setRenderersFactory(
                    playerBuilder, getIntent().getBooleanExtra(PlayerUtil.PREFER_EXTENSION_DECODERS_EXTRA, false));
            mPlayer = playerBuilder.build();

            subtitles= this.findViewById(R.id.leanback_subtitles);
            Intent intent = this.getIntent();
            mPlayer.setTrackSelectionParameters(trackSelectionParameters);
            mPlayer.addListener(new PlayerEventListener());
            mediaItems = createMediaItems(intent, true);
           // mediaItemsMulti = createMediaItems(intent, true);
            mPlayer.addAnalyticsListener(new EventLogger());
            mPlayer.setAudioAttributes(AudioAttributes.DEFAULT, /* handleAudioFocus= */ true);

            mPlayer.setPlayWhenReady(startAutoPlay);
            playerView.setPlayer(mPlayer);
            // configurePlayerWithServerSideAdsLoader();
          //  debugViewHelper = new DebugTextViewHelper(mPlayer, debugTextView);
           // debugViewHelper.start();
            Log.d("IDebug", "Exo player is created" );
        }
        boolean haveStartPosition = startItemIndex != C.INDEX_UNSET;
        if (haveStartPosition) {
            Log.d("IDebug", "Seek to start position" );
            mPlayer.seekTo(startItemIndex, startPosition);
        }
        //mPlayer.setMediaItems(mediaItems, /* resetPosition= */ !haveStartPosition);
        MediaSource mediaSource  = itemsToSource(mediaItems);
        Log.d("IDebug", "Set " + mediaItems.size() + " items into palyer");
       // mPlayer.setMediaItem(mediaItems.get(0), !haveStartPosition);
      //  mPlayer.addMediaItem(mediaItems.get(1));
        mPlayer.setMediaSource(mediaSource);
        Log.d("IDebug", "Prepare player" );
        mPlayer.prepare();

        Log.d("IDebug", "player is prepared" );
        updateButtonVisibility();
        if(!isForceSubtitle){
            subtitles.setVisibility(View.GONE);
        }else{
            subtitles.setVisibility(View.VISIBLE);
        }


        skipIntroButton = this.findViewById(R.id.skip_intro);
        skipIntroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skipIntro(view);
            }
        });
        mPlayer.play();
        Log.d("*******", "play: " + resumeTime + " imtes : " + mPlayer.getMediaItemCount());

        if(resumeTime > 0){
            Log.d("*******", "resume to : " + resumeTime);
            mPlayer.seekTo(resumeTime );
        }
        return true;
    }

    private MediaSource itemsToSource(List<MediaItem> mItems){
        DefaultDataSource.Factory dataSourceFactory = new DefaultDataSource.Factory(this);
        MediaSource mSource;
        if(mimeType.equals(MimeTypes.APPLICATION_M3U8)){
            Log.d(TAG,"items to source >>> create HlsMediaSource");
            mSource = new HlsMediaSource.Factory(dataSourceFactory).
                    createMediaSource(mItems.get(0));
        }else{
            Log.d(TAG,"items to source >>> create Progressive");
            mSource = new ProgressiveMediaSource.Factory(dataSourceFactory).
                    createMediaSource(mItems.get(0));
        }

        MergingMediaSource tMediaSource = new MergingMediaSource(mSource);
        for (int i = 1; i < mediaItems.size(); i++) {
            mSource = new ProgressiveMediaSource.Factory(dataSourceFactory).
                    createMediaSource(mItems.get(i));
            tMediaSource = new MergingMediaSource(tMediaSource, mSource);
        }
        if(subTitleLink!= null && subTitleLink.length() > 0 ){
            Log.d(TAG, "prepareMediaForPlaying: Subtitles: " + subTitleLink );
            DefaultBandwidthMeter bandwidthMeter = DefaultBandwidthMeter.getSingletonInstance(this);

            MediaSource textMediaSource = new SingleSampleMediaSource.Factory(dataSourceFactory).createMediaSource(createSubtitleConfiguration(subTitleLink), C.TIME_UNSET);
             tMediaSource = new MergingMediaSource(tMediaSource, textMediaSource);
            mPlayer.setMediaSource(tMediaSource);
        }
        return tMediaSource;
    };

    @Override
    public void onVisibilityChanged(int visibility) {
       // debugRootView.setVisibility(visibility);
    }

    private class PlayerEventListener implements Player.Listener {

        @Override
        public void onPlaybackStateChanged(@Player.State int playbackState) {
            if (playbackState == Player.STATE_ENDED) {
              //  showControls();
            }
            updateButtonVisibility();
        }

        @Override
        public void onPlayerError(PlaybackException error) {
            if (error.errorCode == PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW) {
                mPlayer.seekToDefaultPosition();
                mPlayer.prepare();
            } else {
                updateButtonVisibility();
               // showControls();
            }
        }

        @Override
        @SuppressWarnings("ReferenceEquality")
        public void onTracksChanged(Tracks tracks) {
          //  updateButtonVisibility();
            if (tracks == lastSeenTracks) {
                return;
            }
            if (tracks.containsType(C.TRACK_TYPE_VIDEO)
                    && !tracks.isTypeSupported(C.TRACK_TYPE_VIDEO, /* allowExceedsCapabilities= */ true)) {
               // showToast(R.string.error_unsupported_video);
            }
            if (tracks.containsType(C.TRACK_TYPE_AUDIO)
                    && !tracks.isTypeSupported(C.TRACK_TYPE_AUDIO, /* allowExceedsCapabilities= */ true)) {
               // showToast(R.string.error_unsupported_audio);
            }
            lastSeenTracks = tracks;
        }

        @Override
        public void onTimelineChanged(Timeline timeline, @Player.TimelineChangeReason int reason)  {

            Log.d(TAG, "onLoadingChanged: ");

        }
    }

    private List<MediaItem> createMediaItems(Intent intent, boolean multi) {
        Log.d("IDebug", "createMediaItems intent" );
        String action = intent.getAction();
        Log.d("IDebug", "action "+action );
        boolean actionIsListView = PlayerUtil.ACTION_VIEW_LIST.equals(action);
       /* if (!actionIsListView && !PlayerUtil.ACTION_VIEW.equals(action)) {
            showToast(getString(R.string.unexpected_intent_action, action));
            this.finish();
            return Collections.emptyList();
        }*/

        List<MediaItem> mediaItems =
                createMediaItems(intent, DemoUtil.getDownloadTracker(/* context= */ this), multi);
        Log.d("IDebug", "Items size "+mediaItems.size() );
        for (int i = 0; i < mediaItems.size(); i++) {
            MediaItem mediaItem = mediaItems.get(i);

            if (!Util.checkCleartextTrafficPermitted(mediaItem)) {
                showToast(R.string.error_cleartext_not_permitted);
                Log.d("IDebug", "error_cleartext_not_permitted " );
                this.finish();
                return Collections.emptyList();
            }
           /* if (Util.maybeRequestReadExternalStoragePermission(*//* activity= *//* this, mediaItem)) {
                // The player will be reinitialized if the permission is granted.
                Log.d("IDebug", "The player will be reinitialized if the permission is granted. " );
                return Collections.emptyList();
            }*/

            MediaItem.DrmConfiguration drmConfiguration = mediaItem.localConfiguration.drmConfiguration;
            if (drmConfiguration != null) {
                if (Build.VERSION.SDK_INT < 18) {
                    //showToast(R.string.error_drm_unsupported_before_api_18);
                    this.finish();
                    return Collections.emptyList();
                } else if (!FrameworkMediaDrm.isCryptoSchemeSupported(drmConfiguration.scheme)) {
                    //showToast(R.string.error_drm_unsupported_scheme);
                    this.finish();
                    return Collections.emptyList();
                }
            }
        }
        return mediaItems;
    }

    private static List<MediaItem> createMediaItems(Intent intent, DownloadTracker downloadTracker, boolean multi) {
        Log.d("IDebug", "Create media items" );
        List<MediaItem> mediaItems = new ArrayList<>();
        for (MediaItem item : PlayerUtil.createMediaItemsFromIntent(intent, multi)) {
            Log.d("IDebug", "Add media item" );
            mediaItems.add(
                    maybeSetDownloadProperties(
                            item, downloadTracker.getDownloadRequest(item.localConfiguration.uri)));
        }
        return mediaItems;
    }

    private static MediaItem maybeSetDownloadProperties(
            MediaItem item, @Nullable DownloadRequest downloadRequest) {
        if (downloadRequest == null) {
            return item;
        }
        MediaItem.Builder builder = item.buildUpon();
        builder
                .setMediaId(downloadRequest.id)
                .setUri(downloadRequest.uri)
                .setCustomCacheKey(downloadRequest.customCacheKey)
                .setMimeType(downloadRequest.mimeType)
                .setStreamKeys(downloadRequest.streamKeys);
        @Nullable
        MediaItem.DrmConfiguration drmConfiguration = item.localConfiguration.drmConfiguration;
        if (drmConfiguration != null) {
            builder.setDrmConfiguration(
                    drmConfiguration.buildUpon().setKeySetId(downloadRequest.keySetId).build());
        }
        return builder.build();
    }

    private class PlayerErrorMessageProvider implements ErrorMessageProvider<PlaybackException> {

        @Override
        public Pair<Integer, String> getErrorMessage(PlaybackException e) {
            String errorString = getString(R.string.error_generic);
            Throwable cause = e.getCause();
            if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                // Special case for decoder initialization failures.
                MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                        (MediaCodecRenderer.DecoderInitializationException) cause;
                if (decoderInitializationException.codecInfo == null) {
                    if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                        errorString = getString(R.string.error_querying_decoders);
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString =
                                getString(
                                        R.string.error_no_secure_decoder, decoderInitializationException.mimeType);
                    } else {
                        errorString =
                                getString(R.string.error_no_decoder, decoderInitializationException.mimeType);
                    }
                } else {
                    errorString =
                            getString(
                                    R.string.error_instantiating_decoder,
                                    decoderInitializationException.codecInfo.name);
                }
            }
            return Pair.create(0, errorString);
        }
    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void updateButtonVisibility() {
        selectTracksButton.setEnabled(mPlayer != null && TrackSelectionDialog.willHaveContent(mPlayer));
        long currPosition = mPlayer.getCurrentPosition();
        if (currPosition > introTimeSec * 1000){
            skipIntroButton.setVisibility(View.GONE);
        }
    }

    private void setTime(long contentPosition, long duration){

        Retrofit apiClient = ApiClient.getClient();
        SessionManager sessionManager = new SessionManager(this);
        VideoParams requestParam = new VideoParams();
        requestParam.setDuration(duration/1000);
        requestParam.setVideo_id(video_id);
        requestParam.setTime(contentPosition/1000);
        requestParam.setSlug(mCurrentSlug);
        Call<TokenResult> service = apiClient.create(Service.class).setTime(mCurrentSlug,requestParam, sessionManager.getToken());
        service.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {
                Log.d(TAG,response.message());

            }
            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {
                Log.d(TAG,t.getMessage());

            }
        });


    }

    public void skipIntro(View view) {
        Log.d(TAG, "Skip intro to " + introTimeSec);
        mPlayer.seekTo((introTimeSec* 1000L));
        skipIntroButton.setVisibility(View.GONE);
    }

    private void releasePlayer() {
        if (mPlayer != null) {
            setTime(mPlayer.getCurrentPosition(), mPlayer.getDuration());
            mPlayer.release();
            mPlayer = null;
        }
    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        releasePlayer();
        setIntent(intent);
    }

    private MediaSource.Factory createMediaSourceFactory() {
        DefaultDrmSessionManagerProvider drmSessionManagerProvider =
                new DefaultDrmSessionManagerProvider();
        drmSessionManagerProvider.setDrmHttpDataSourceFactory(
                DemoUtil.getHttpDataSourceFactory(/* context= */ this));

        return new DefaultMediaSourceFactory(/* context= */ this)
                .setDataSourceFactory(dataSourceFactory)
                .setDrmSessionManagerProvider(drmSessionManagerProvider);
    }

    private void setRenderersFactory(
            ExoPlayer.Builder playerBuilder, boolean preferExtensionDecoders) {
        RenderersFactory renderersFactory =
                DemoUtil.buildRenderersFactory(/* context= */ this, preferExtensionDecoders);
        playerBuilder.setRenderersFactory(renderersFactory);
    }

    private  MediaItem.SubtitleConfiguration createSubtitleConfiguration(
            String url) {
        Log.d("PlayerUtils", "createSubtitleConfiguration");

        int subFlag = isForceSubtitle ? C.SELECTION_FLAG_DEFAULT : C.SELECTION_FLAG_AUTOSELECT;
        return new MediaItem.SubtitleConfiguration.Builder(
                Uri.parse(url))
                .setMimeType(MimeTypes.TEXT_VTT)
                .setLanguage("de")
                .setSelectionFlags(subFlag)
                .build();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT <= 23 || mPlayer == null) {
            Log.d("IDebug", "Call initialize player on resume" );
            initializePlayer();
            if (playerView != null) {
                playerView.onResume();
            }
        }
    }
}

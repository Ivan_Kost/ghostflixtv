package io.ghostflix.tv.network;




import io.ghostflix.tv.model.Login;
import io.ghostflix.tv.model.content.AWSUrls;
import io.ghostflix.tv.model.content.ContentResult;
import io.ghostflix.tv.model.episode.EpisodeResult;
import io.ghostflix.tv.model.user.RequestParam;
import io.ghostflix.tv.model.user.TokenResult;
import io.ghostflix.tv.model.user.UserContentResult;
import io.ghostflix.tv.model.user.VideoParams;
import io.ghostflix.tv.model.video.PlayerConfigResult;

import java.util.List;

import io.ghostflix.tv.model.video.SignedUrlsRequest;
import io.ghostflix.tv.model.video.SignedUrlsResult;
import io.ghostflix.tv.model.video.TextTracksApiResult;
import io.ghostflix.tv.model.video.VideoApiResult;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Service {

    @POST("wp-json/jwt-auth/v1/token")
    Call<TokenResult> getToken(@Body
                                       Login post);

    @POST("wp-json/api/v1/watch-list/add")
    Call<TokenResult> addToWatchList(@Body RequestParam post, @Header("Authorization") String authHeader);

    @POST("/wp-json/api/v1/watch-list/remove")
    Call<TokenResult> removeFromWatchList(@Body RequestParam post, @Header("Authorization") String authHeader);

    @POST("wp-json/api/v1/favorites/add")
    Call<TokenResult> addToFavourite(@Body RequestParam post, @Header("Authorization") String authHeader);

    @POST("wp-json/api/v1/favorites/remove")
    Call<TokenResult> removeFromFavourite(@Body RequestParam post, @Header("Authorization") String authHeader);


    @POST("wp-json/api/v1/watched-videos/{slug}/set-time")
    Call<TokenResult> setTime(@Path("slug") String type,@Body
            VideoParams post, @Header("Authorization") String authHeader);

    @POST("/wp-json/api/v1/watched-videos/{$slug}/add")
    Call<TokenResult> watchedVideo(@Path("slug") String type, @Body
            Login post, @Header("Authorization") String authHeader);



    @POST("wp-json/jwt-auth/v1/token/validate")
    Call<TokenResult> validateToken(@Header("Authorization") String authHeader);

    @GET("wp-json/api/v1/videos/{type}")
    Call<List<ContentResult>> getSeriesResutl(@Path("type") String type, @Header("Authorization") String authHeader);

    @GET("wp-json/api/v1/tv-top-ten")
    Call<ContentResult> getTop10( @Header("Authorization") String authHeader);

    @GET("wp-json/api/v1/tv-top-ten-spirit")
    Call<ContentResult> getTop10Spirit( @Header("Authorization") String authHeader);


    @GET("wp-json/api/v1/videos/{slug}/series")
    Call<EpisodeResult> getSeriesDetail(@Path("slug") String slug, @Header("Authorization") String authHeader);

    @GET("wp-json/api/v1/user/data")
    Call<UserContentResult> getUserData(@Header("Authorization") String authHeader);

    @GET
    Call<PlayerConfigResult> getVideoFeed(@Url String feedURL, @Header("Authorization") String authHeader);

    @GET
    Call<PlayerConfigResult> getSubtitles(@Url String subURL, @Header("Authorization") String authHeader);

    @GET
    Call<VideoApiResult> getVideoApiResult(@Url String feedURL, @Header("Authorization") String authHeader);

    @GET
    Call<TextTracksApiResult> getApiSubtitlesResult(@Url String subURL, @Header("Authorization") String authHeader);

    @GET("wp-json/api/v1/search")
    Call<ContentResult> getSearchResult(@Query("q") String type, @Header("Authorization") String authHeader);

    @POST("wp-json/api/v1/sign-url")
    Call<SignedUrlsResult> getAwsUrls(@Body SignedUrlsRequest post, @Header("Authorization") String authHeader);
}

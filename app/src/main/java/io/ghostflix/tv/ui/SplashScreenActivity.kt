package io.ghostflix.tv.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import io.ghostflix.tv.R


class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lateinit var videoView: VideoView
        val videoUrl = "Paste Your Video URL Here"

        setContentView(R.layout.videosplash)

        // on below line we are initializing our variables.
        videoView = findViewById(R.id.videoView)

        // Uri object to refer the
        // resource from the videoUrl
        val uri = Uri.parse(videoUrl)
        val path = "android.resource://" + getPackageName() + "/"+R.raw.start_intro;


        // sets the resource from the
        // videoUrl to the videoView
        videoView.setVideoPath(path)
        videoView.setOnCompletionListener { mp -> // not playVideo
            // playVideo();
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        // starts the video
        videoView.start();


        //startActivity(Intent(this, HomeActivity::class.java))
        // finish()
    }
}
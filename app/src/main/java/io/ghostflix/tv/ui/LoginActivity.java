package io.ghostflix.tv.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.Login;
import io.ghostflix.tv.model.user.TokenResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends BaseActivity {

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText userName = findViewById(R.id.editTextTextPersonName);
        final EditText userPass = findViewById(R.id.editTextTextPersonPassword);
        Button buttonSignIn = findViewById(R.id.button_sign);

        sessionManager = new SessionManager(getApplicationContext());
       // sessionManager.clearSharedPref();

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userName.getText().length() == 0 || userPass.getText().length() ==0){
                    Toast.makeText(getApplicationContext(), "Field can not be empty.", Toast.LENGTH_LONG).show();
                }else{
                    Retrofit apiClient = ApiClient.getClient();

                    Login login = new Login();
                    login.setUsername(userName.getText().toString());
                    login.setPassword(userPass.getText().toString());
                    Call<TokenResult> service = apiClient.create(Service.class).getToken(login);

                    service.enqueue(new Callback<TokenResult>() {
                        @Override
                        public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

                            if(response.body()!= null){
                                SessionManager sessionManager = new SessionManager(getApplicationContext());
                                sessionManager.saveLoginStatus(response.body());
//                                VerifyToken(response.body().getToken());
                                startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                                finish();
                            }

                        }

                        @Override
                        public void onFailure(Call<TokenResult> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Something went wrong.Please try later.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        if(sessionManager.getLoginStatus()){

            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
            finish();
        }
    }
}
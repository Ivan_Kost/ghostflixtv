package io.ghostflix.tv.ui;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;


import com.bumptech.glide.Glide;

import io.ghostflix.tv.R;
import io.ghostflix.tv.adapter.CastRowAdapter;
import io.ghostflix.tv.fragmentui.DetailFragment;
import io.ghostflix.tv.model.Login;
import io.ghostflix.tv.model.episode.EpisodeResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SpiritDetailActivity extends DetailActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spirit_detail);

        mDetailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.s_details_fragment);
        if (mDetailFragment != null) {
            mDetailFragment.setFavouriteItemViewSelectedListener(this);
        }

        mSessionManager = new SessionManager(SpiritDetailActivity.this);
        horizontalGridView = findViewById(R.id.genre_list);
        buttonHorizontalGridView = findViewById(R.id.button_tab_list);
        mTextTitle = findViewById(R.id.title_s);
        mTextDesc = findViewById(R.id.description_s);
        mBackImageView = findViewById(R.id.background_image_view);
        mProgressBar = findViewById(R.id.progressBar);

        buttonHorizontalGridView.requestFocus();
    }

    @Override
    protected void getEpisodeList(String slug) {

        Retrofit apiClient = ApiClient.getClient();

        Login login = new Login();
        Call<EpisodeResult> service = apiClient.create(Service.class).getSeriesDetail(slug, mSessionManager.getToken());

        service.enqueue(new Callback<EpisodeResult>() {
            @Override
            public void onResponse(Call<EpisodeResult> call, Response<EpisodeResult> response) {

                if(response.body()!= null){


                    Log.d(TAG, "onResponse(Spirit): "+response.body().getTitle());
                    mTextTitle.setText(Html.fromHtml(response.body().getTitle()));
                    String description = response.body().getContent();

                    mTextDesc.setText(Html.fromHtml(description));

                    mCurrentEpisodeId = response.body().getId().toString();
                    mCuurentSlug = response.body().getSlug();

                    if(response.body().getGenres()!= null && response.body().getGenres().size() >0){
                        horizontalGridView.setAdapter(new CastRowAdapter(response.body().getGenres(), SpiritDetailActivity.this));
                    }
                    Glide.with(getApplicationContext())
                            .load(response.body().getHeader_background())
                            .into(mBackImageView);

                    if(response.body().getEpisodes()!= null){
                        mDetailFragment.displaySpiritBottomList(response.body().getSlug(),response.body().getEpisodes());
                    }

                }
            }

            @Override
            public void onFailure(Call<EpisodeResult> call, Throwable t) {
                Log.d(TAG, "Spirit onFailure: "+t.getCause());
                Log.d(TAG, "onFailure: "+t.getMessage());
                Log.d(TAG, "onFailure: "+t.getLocalizedMessage());
                Log.d(TAG, "onFailure: "+call);

//                Toast.makeText(getApplicationContext(), "Something went wrong.Please try later.", Toast.LENGTH_LONG).show();
            }
        });
    }
}

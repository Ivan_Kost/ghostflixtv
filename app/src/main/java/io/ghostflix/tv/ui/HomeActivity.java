package io.ghostflix.tv.ui;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.widget.HorizontalGridView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import io.ghostflix.tv.R;
import io.ghostflix.tv.adapter.CategoryAdapter;
import io.ghostflix.tv.adapter.SpiritCategoryAdapter;
import io.ghostflix.tv.fragmentui.HomeFragment;
import io.ghostflix.tv.fragmentui.ImprintFragment;
import io.ghostflix.tv.fragmentui.PrivacyAcceptFragment;
import io.ghostflix.tv.fragmentui.PrivacyFragment;
import io.ghostflix.tv.fragmentui.ProfileFragment;
import io.ghostflix.tv.fragmentui.SearchFragment;
import io.ghostflix.tv.fragmentui.SpiritFragment;
import io.ghostflix.tv.model.Login;
import io.ghostflix.tv.model.content.ContentResult;
import io.ghostflix.tv.model.user.TokenResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeActivity extends BaseActivity {

    private HorizontalGridView mTopNavbar;
    private FrameLayout mHomeContainer;
    private SessionManager mSessionManager;
    private SpiritFragment spiritFragment;
    private HomeFragment homeFragment;
    private SearchFragment searchFragment;
    private ProfileFragment profileFragment;
    private PrivacyFragment privacyFragment;
    private PrivacyAcceptFragment privacyAcceptFragment;
    private ImageView mLogo;
    private CategoryAdapter cadapter;
    private SpiritCategoryAdapter spAdapter;
    private ImprintFragment imprintFragment;
    private RelativeLayout mTopBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mTopBar = findViewById(R.id.top_nav_bar);
        mTopNavbar = findViewById(R.id.top_tab_list);
        mLogo = mTopBar.findViewById(R.id.logo);
        mHomeContainer = findViewById(R.id.home_container);
        mSessionManager = new SessionManager(HomeActivity.this);
        mTopNavbar.setWindowAlignment(HorizontalGridView.WINDOW_ALIGN_BOTH_EDGE);
        mTopNavbar.setWindowAlignmentOffsetPercent(35);
        mTopNavbar.setSelectedPosition(0);
        ArrayList<String> topNavList = new ArrayList<>();
//        topNavList.add("Serien & Dokumentationen");
        topNavList.add("Paranormales");
 //       topNavList.add("Neu");
//        topNavList.add("Kommt bald");
        topNavList.add("Spirit Academy");
        topNavList.add("Se");
        topNavList.add("Pro");
        homeFragment = new HomeFragment();
        spiritFragment = new SpiritFragment();
        searchFragment = new SearchFragment();
        profileFragment= new ProfileFragment();



        CategoryAdapter.OnTopRowClickListener topListener = new CategoryAdapter.OnTopRowClickListener() {
            @Override
            public void onItemClick(String selectedText) {

                if(selectedText.equalsIgnoreCase("Paranormales")){
                    replaceFragment(homeFragment, "home");
                    getSeriesContent("staffeln");
                    getTop10Content("home");
                }else if (selectedText.equalsIgnoreCase("Neu")){
                    replaceFragment(homeFragment, "home");
                    getSeriesContent("neu");
                }else if(selectedText.equalsIgnoreCase("Kommt bald")){
                    replaceFragment(homeFragment, "home");
                    getSeriesContent("kommt-bald");
                }else if(selectedText.equalsIgnoreCase("Spirit Academy")){
                    adjustSpiritLayout();
                    replaceFragment(spiritFragment, "home");
                    getSeriesContent("spirit-academy");
                    getTop10Content("spirit");
                }else if(selectedText.equalsIgnoreCase("Se")){
                    adjustLayout();
                    replaceFragment(searchFragment, "home");
                }else if(selectedText.equalsIgnoreCase("Pro")){
                    replaceFragment(profileFragment, "home");
                }else if(selectedText.equalsIgnoreCase("Privacy")){
                    privacyFragment= new PrivacyFragment();
                    replaceFragment(privacyFragment, "home");
                }
            }
        };
        this.cadapter = new CategoryAdapter(topNavList, HomeActivity.this,topListener);
        this.spAdapter = new SpiritCategoryAdapter(topNavList, HomeActivity.this,topListener);

        mTopNavbar.setAdapter(this.cadapter);
        mTopNavbar.setSelectedPosition(0);
        VerifyToken();
        replaceFragment(homeFragment, "home");
    }

    private void adjustLayout(){
        mLogo.setImageResource(R.drawable.flix);
        mTopNavbar.setAdapter(cadapter);
    }


    private void adjustSpiritLayout(){
        cadapter.adjustCatogoryLayout("spirit");
        mLogo.setImageResource(R.drawable.spirit_logo);
        mTopNavbar.setAdapter(spAdapter);
    }

    private void VerifyToken() {

        Retrofit apiClient = ApiClient.getClient();

        Call<TokenResult> service = apiClient.create(Service.class).validateToken(mSessionManager.getToken());
        service.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

                if(response.body()!= null){
                    Log.d("Response", "onResponse: "+response.body());
                    if(response.body().getCode().equalsIgnoreCase("jwt_auth_valid_token")){
                        getSeriesContent("staffeln");
                        getTop10Content("home");
                    }
                }else{
                    mSessionManager.clearSharedPref();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }

            }

            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {
              //  Toast.makeText(getApplicationContext(), "Something went wrong.Please try later.", Toast.LENGTH_LONG).show();
                mSessionManager.clearSharedPref();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });
    }

    private void getSeriesContent(String typeContent) {

        Retrofit apiClient = ApiClient.getClient();

        Login login = new Login();
        Call<List<ContentResult>> service = apiClient.create(Service.class).getSeriesResutl(typeContent, mSessionManager.getToken());

        service.enqueue(new Callback<List<ContentResult>>() {
            @Override
            public void onResponse(Call<List<ContentResult>> call, Response<List<ContentResult>> response) {

                if(response.body()!= null && response.body().size() >0 ){
                    if(typeContent.equals("spirit-academy")){
                        spiritFragment.setListContent(response.body());
                    }else{
                        homeFragment.setListContent(response.body());
                    }

                }
            }

            @Override
            public void onFailure(Call<List<ContentResult>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong.Please try later.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getTop10Content(String typeContent) {

        Retrofit apiClient = ApiClient.getClient();
        Call<ContentResult> service;
        Login login = new Login();
        if (typeContent.equals("spirit")) {
             service = apiClient.create(Service.class).getTop10Spirit( mSessionManager.getToken());
        } else {
             service = apiClient.create(Service.class).getTop10( mSessionManager.getToken());
        }

        service.enqueue(new Callback<ContentResult>() {
            @Override
            public void onResponse(Call<ContentResult> call, Response<ContentResult> response) {
                if(response.body()!= null  ){
                    if(typeContent.equals("spirit")){
                        spiritFragment.setTopListContent(response.body());
                    }else{
                        homeFragment.setTopListContent(response.body());
                    }

                }
            }

            @Override
            public void onFailure(Call<ContentResult> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong.Please try later.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void replaceFragment(Fragment fragment, String name) {
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.home_container, fragment, name);
        ft.commit();
    }

}
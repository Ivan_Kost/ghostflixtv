package io.ghostflix.tv.ui;

import android.content.Intent;

import androidx.fragment.app.FragmentActivity;

public class LeanbackActivity extends FragmentActivity {
    @Override
    public boolean onSearchRequested() {
        startActivity(new Intent(this, SearchActivity.class));
        return true;
    }
}
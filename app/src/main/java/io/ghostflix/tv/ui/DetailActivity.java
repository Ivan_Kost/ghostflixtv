package io.ghostflix.tv.ui;

import androidx.leanback.widget.HorizontalGridView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import io.ghostflix.tv.R;
import io.ghostflix.tv.adapter.ButtonAdapter;
import io.ghostflix.tv.adapter.CastRowAdapter;
import io.ghostflix.tv.fragmentui.DetailFragment;
import io.ghostflix.tv.media.ExoPlayerActivity;
import io.ghostflix.tv.model.Login;
import io.ghostflix.tv.model.content.AWSUrls;
import io.ghostflix.tv.model.content.VideoItem;
import io.ghostflix.tv.model.episode.EpisodeContent;
import io.ghostflix.tv.model.episode.EpisodeResult;
import io.ghostflix.tv.model.user.RequestParam;
import io.ghostflix.tv.model.user.TokenResult;
import io.ghostflix.tv.model.user.UserContentResult;
import io.ghostflix.tv.model.video.SignedUrlsRequest;
import io.ghostflix.tv.model.video.SignedUrlsResult;
import io.ghostflix.tv.model.video.TextTracksApiResult;
import io.ghostflix.tv.model.video.VideoApiResult;
import io.ghostflix.tv.network.ApiClient;
import io.ghostflix.tv.network.Service;
import io.ghostflix.tv.utils.PictureUtils;
import io.ghostflix.tv.utils.PlayerHelper;
import io.ghostflix.tv.utils.PlayerUtil;
import io.ghostflix.tv.utils.SessionManager;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailActivity extends BaseActivity implements DetailFragment.DetailFragmentItemSelectedListener {

    protected static final String TAG = DetailActivity.class.getName();
    protected TextView mTextTitle;
    protected TextView mTextDesc;
    protected ImageView mBackImageView;
    protected DetailFragment mDetailFragment;


    protected SessionManager mSessionManager;
    protected HorizontalGridView horizontalGridView;
    protected HorizontalGridView buttonHorizontalGridView;
    protected String mCurrentEpisodeId;
    protected EpisodeContent mCurrentEpisodeContent;
    protected String mCuurentSlug;
    protected ProgressBar mProgressBar;
    protected long mResumeTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mDetailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.details_fragment);
        if (mDetailFragment != null) {
            mDetailFragment.setFavouriteItemViewSelectedListener(this);
        }

        mSessionManager = new SessionManager(DetailActivity.this);
        horizontalGridView = findViewById(R.id.genre_list);
        buttonHorizontalGridView = findViewById(R.id.button_tab_list);
        mTextTitle = findViewById(R.id.title_s);
        mTextDesc = findViewById(R.id.description_s);
        mBackImageView = findViewById(R.id.background_image_view);
        mProgressBar = findViewById(R.id.progressBar);

        buttonHorizontalGridView.requestFocus();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getEpisodeList(getIntent().getStringExtra("slug"));
    }

    protected void getEpisodeList(String slug) {

        Retrofit apiClient = ApiClient.getClient();

        Login login = new Login();
        Call<EpisodeResult> service = apiClient.create(Service.class).getSeriesDetail(slug, mSessionManager.getToken());

        service.enqueue(new Callback<EpisodeResult>() {
            @Override
            public void onResponse(Call<EpisodeResult> call, Response<EpisodeResult> response) {

                if(response.body()!= null){


                    Log.d(TAG, "onResponse: "+response.body().getTitle());
                    mTextTitle.setText(Html.fromHtml(response.body().getTitle()));
                    String description = response.body().getContent();

                    mTextDesc.setText(Html.fromHtml(description));
                    mCurrentEpisodeId = response.body().getId().toString();
                    mCuurentSlug = response.body().getSlug();

                    if(response.body().getGenres()!= null && response.body().getGenres().size() >0){
                        horizontalGridView.setAdapter(new CastRowAdapter(response.body().getGenres(), DetailActivity.this));
                    }
                    Glide.with(getApplicationContext())
                            .load(response.body().getHeader_background())
                            .into(mBackImageView);

                    if(response.body().getEpisodes()!= null){
                        mDetailFragment.displayBottomList(response.body().getSlug(),response.body().getEpisodes());
                    }

                }
            }

            @Override
            public void onFailure(Call<EpisodeResult> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getCause());
                Log.d(TAG, "onFailure: "+t.getMessage());
                Log.d(TAG, "onFailure: "+t.getLocalizedMessage());
                Log.d(TAG, "onFailure: "+call);

//                Toast.makeText(getApplicationContext(), "Something went wrong.Please try later.", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onSelected(EpisodeContent categoryItem) {

        mCurrentEpisodeContent = categoryItem;

        if(categoryItem.getEpisodeTitle() != null){
            mTextTitle.setText(categoryItem.getEpisodeTitle());
        }

        if (categoryItem.getDescription() != null){
            String description  = categoryItem.getDescription();
            if(categoryItem.isLocked()){
                description += " <span style=\"color:#ffbe00\"> Subscription is required!</span>";
            }
            mTextDesc.setText(Html.fromHtml(description));
        }
        if(categoryItem.getProgress() > 0){
            mProgressBar.setProgress((int) categoryItem.getProgress());
        }else{
            mProgressBar.setProgress(0);
        }
        mResumeTime = 0;
        if ( !categoryItem.isIs_watched() && categoryItem.getTime() != null && categoryItem.getTime() > 0) {

            Log.d(TAG, "onSelected: set time");
            Log.d(TAG, categoryItem.getTime() + "");

            try {
                long time = categoryItem.getTime();
                mResumeTime = time;
            } catch (NumberFormatException e){
                mResumeTime = 0;
            }
            Log.d(TAG, " set time 2 " + mResumeTime);
        } else {
           // mResumeTime = categoryItem.getProgress();

        }

        if((categoryItem.getVimeoVideo() == null && categoryItem.getMp4_video() == null ) || categoryItem.isLocked()){
            buttonHorizontalGridView.setVisibility(View.GONE);
        }else{
            buttonHorizontalGridView.setVisibility(View.VISIBLE);
        }

        getUserContent();
    }

    private void getUserContent() {

        Retrofit apiClient = ApiClient.getClient();

        SessionManager sessionManager = new SessionManager(DetailActivity.this);
        Call<UserContentResult> service = apiClient.create(Service.class).getUserData( sessionManager.getToken());
        service.enqueue(new Callback<UserContentResult>() {
            @Override
            public void onResponse(Call<UserContentResult> call, Response<UserContentResult> response) {

                if(response.body()!=null){

                    ArrayList<String> topNavList = new ArrayList<>();
                    List<VideoItem> watchList = response.body().getWishlist();
                    List<VideoItem> favoritenList = response.body().getFavoriten();

                    String watchListTitle = "Add to Watchlist";
                    String favouriteTitle = "Add to Favourite";
                    for (VideoItem item: watchList
                         ) {
                        if(mCurrentEpisodeId.equalsIgnoreCase(item.getId().toString())){
                            watchListTitle = "Remove from Watchlist";
                        }
                    }
                    for (VideoItem item: favoritenList
                    ) {
                        if(mCurrentEpisodeId.equalsIgnoreCase(item.getId().toString())){
                            favouriteTitle = "Remove to Favourite";
                        }
                    }
                    topNavList.add("Abspielen");
                    if(mResumeTime > 0 ){
                        topNavList.add("Fortsetzen");
                    }
                    topNavList.add(watchListTitle);
                    topNavList.add(favouriteTitle);

                    showTopNav(topNavList);
                }
            }
            @Override
            public void onFailure(Call<UserContentResult> call, Throwable t) {


            }
        });
    }

    private void showTopNav(ArrayList<String> topNavList) {
        buttonHorizontalGridView.setAdapter(new ButtonAdapter(topNavList, DetailActivity.this, selectedText -> {

            if(selectedText.equalsIgnoreCase("Add to Watchlist")){
                addToWatchList();
            }else if(selectedText.equalsIgnoreCase("Remove from Watchlist")){
                removeToWatchList();
            }else if(selectedText.equalsIgnoreCase("Add to Favourite")){
                addToFavourite();
            } else if(selectedText.equalsIgnoreCase("Remove to Favourite")){
                removeFromFavourite();
            }else if(selectedText.equalsIgnoreCase("Abspielen")){
                playContent();
            }else if(selectedText.equalsIgnoreCase("Fortsetzen")){
                playContent();
            }

        }));

    }

    private void playContent() {
        Retrofit apiClient = ApiClient.getClient();
        String mp4UrlSource = mCurrentEpisodeContent.getMp4_video();
        String hlsUrlSource = mCurrentEpisodeContent.getHls_video();
        Log.d("MSG", "debug: check if Episode content is locked!");

        if(mCurrentEpisodeContent.isLocked()){
            Log.d("MSG", "debug: Episode content is locked!");
            return;
        }
        if(hlsUrlSource != null && !hlsUrlSource.equals("")) {
            Intent intent = PlayerHelper.createHLSVideoIntent(mCurrentEpisodeContent, DetailActivity.this, mCuurentSlug,hlsUrlSource,mCurrentEpisodeContent.getTextrack());
            intent.putExtra("resume",mResumeTime);
            startActivity(intent);
        }else if(mp4UrlSource != null && !mp4UrlSource.equals("")) {
            SessionManager sessionManager = new SessionManager(DetailActivity.this);
            AWSUrls awsUrls = new AWSUrls();
            awsUrls.setSdVideo(mCurrentEpisodeContent.getMp4_video());
            awsUrls.setHdVideo(mCurrentEpisodeContent.getMp4_video_hd());
            awsUrls.setTextTrack(mCurrentEpisodeContent.getTextrack());
            SignedUrlsRequest signedUrlsRequest = new SignedUrlsRequest();
            signedUrlsRequest.setAws_url(awsUrls);
            Call<SignedUrlsResult> service = apiClient.create(Service.class).getAwsUrls(signedUrlsRequest,sessionManager.getToken());
            service.enqueue(new Callback<SignedUrlsResult>() {
                @Override
                public void onResponse(Call<SignedUrlsResult> call, Response<SignedUrlsResult> response) {
                    if (response != null && response.body() != null) {
                        SignedUrlsResult signedUrlsResult = response.body();
                        Intent intent = PlayerHelper.createVideoIntent(mCurrentEpisodeContent, DetailActivity.this, mCuurentSlug,signedUrlsResult.getSigned_url());
                        intent.putExtra("resume",mResumeTime);
                        startActivity(intent);
                    }
                }
                @Override
                public void onFailure(Call<SignedUrlsResult> call, Throwable t) {
                    Log.d("MSG", "onFailure: " + t.getLocalizedMessage());
                    Log.d("MSG", "onFailure: " + t.getMessage());
                    Log.d("MSG", "onFailure: " + t.getCause());
                }
            });
        }else{
            String apiUrl = "https://api.vimeo.com/videos/"+ mCurrentEpisodeContent.getVimeoVideo();
            String textTrackApiUrl = apiUrl + "/texttracks";
            String header = "Bearer fd9ba317a092fde3ded0c9d6f04fa89b";
            Intent intent = new Intent(DetailActivity.this, ExoPlayerActivity.class);
            final boolean[] videoIsReady = {false};
            final boolean[] textTrackIsReady = {false};
            Call<VideoApiResult> service = apiClient.create(Service.class).getVideoApiResult(apiUrl, header);
            Call<TextTracksApiResult> textTrackService = apiClient.create(Service.class).getApiSubtitlesResult(textTrackApiUrl,header);
            service.enqueue(new Callback<VideoApiResult>() {
                @Override
                public void onResponse(Call<VideoApiResult> call, Response<VideoApiResult> response) {

                    if (response != null && response.body() != null) {
                        VideoApiResult channelResponse = response.body();
                        // Intent intent = new Intent(getActivity(), ExoPlayerActivity.class);
                        intent.putExtra("VideoTitle", mCurrentEpisodeContent.getEpisodeTitle());
                        intent.putExtra("VidDesc", mCurrentEpisodeContent.getDescription());
                        intent.putExtra("VideoLink", PictureUtils.getApiVideo(channelResponse.getFiles()));
                        intent.putExtra("videoLinksArray",PictureUtils.getVimeoArray(channelResponse.getFiles()));
                        intent.putExtra(PlayerUtil.MIME_TYPE_EXTRA,"video/mp4");
                        intent.putExtra(PlayerUtil.TITLE_EXTRA,mCurrentEpisodeContent.getEpisodeTitle());
                        // intent.putExtra("VideoLink", episodeContent.getMp4_video());
                        // intent.putExtra("subTitleLink", PictureUtils.getSubtitle(channelResponse.getRequest().getTextTracksList()));
                        if (mCurrentEpisodeContent.getForceSubtitles() != null && mCurrentEpisodeContent.getForceSubtitles().equalsIgnoreCase("on")) {
                            intent.putExtra("force_subtitle", true);
                        }
                        //   intent.putExtra("vimeoId", vimeoId);
                        intent.putExtra("slug", mCuurentSlug);
                        intent.putExtra("video_id", mCurrentEpisodeContent.getVideo_id());
                        intent.putExtra("skipIntroTime",mCurrentEpisodeContent.getSkipIntroTime());
                        intent.putExtra("resume",mResumeTime);
                        videoIsReady[0] = true;
                        if (textTrackIsReady[0]){
                            startActivity(intent);
                        }

                    }
                }

                @Override
                public void onFailure(Call<VideoApiResult> call, Throwable t) {

                    Log.d("MSG", "onFailure: " + t.getLocalizedMessage());
                    Log.d("MSG", "onFailure: " + t.getMessage());
                    Log.d("MSG", "onFailure: " + t.getCause());
                }
            });

            textTrackService.enqueue(new Callback<TextTracksApiResult>() {
                @Override
                public void onResponse(Call<TextTracksApiResult> call, Response<TextTracksApiResult> response) {

                    if (response != null && response.body() != null) {
                        TextTracksApiResult channelResponse = response.body();
                        intent.putExtra("subTitleLink", PictureUtils.getTextTrack(channelResponse.getTextTracks()));
                        intent.putExtra(PlayerUtil.SUBTITLE_URI_EXTRA,PictureUtils.getTextTrack(channelResponse.getTextTracks()));
                        if (mCurrentEpisodeContent.getForceSubtitles() != null && mCurrentEpisodeContent.getForceSubtitles().equalsIgnoreCase("on")) {
                            intent.putExtra("force_subtitle", true);
                        }
                        //   intent.putExtra("vimeoId", vimeoId);
                        textTrackIsReady[0] = true;
                        if (videoIsReady[0]){
                            startActivity(intent);
                        }

                    }
                }

                @Override
                public void onFailure(Call<TextTracksApiResult> call, Throwable t) {

                    Log.d("MSG", "onFailure: " + t.getLocalizedMessage());
                    Log.d("MSG", "onFailure: " + t.getMessage());
                    Log.d("MSG", "onFailure: " + t.getCause());
                }
            });
        }



    }

    private void addToWatchList() {


        Retrofit apiClient = ApiClient.getClient();
        SessionManager sessionManager = new SessionManager(DetailActivity.this);
        RequestParam requestParam = new RequestParam();
        requestParam.setId(mCurrentEpisodeId);
        Call<TokenResult> service = apiClient.create(Service.class).addToWatchList(requestParam, sessionManager.getToken());
        service.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

                if (response.body() != null && response.body().isSuccess()) {
                    getUserContent();
                }

            }
            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {


            }
        });

    }
    private void removeToWatchList() {

        Retrofit apiClient = ApiClient.getClient();
        SessionManager sessionManager = new SessionManager(DetailActivity.this);
        RequestParam requestParam = new RequestParam();
        requestParam.setId(mCurrentEpisodeId);
        Call<TokenResult> service = apiClient.create(Service.class).removeFromWatchList(requestParam, sessionManager.getToken());
        service.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

                if (response.body() != null && response.body().isSuccess()) {
                    getUserContent();
                }

            }
            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {


            }
        });
    }
    private void addToFavourite() {

        Retrofit apiClient = ApiClient.getClient();
        SessionManager sessionManager = new SessionManager(DetailActivity.this);
        RequestParam requestParam = new RequestParam();
        requestParam.setId(mCurrentEpisodeId);
        Call<TokenResult> service = apiClient.create(Service.class).addToFavourite(requestParam, sessionManager.getToken());
        service.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

                if (response.body() != null && response.body().isSuccess()) {
                    getUserContent();
                }

            }
            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {


            }
        });
    }
    private void removeFromFavourite() {

        Retrofit apiClient = ApiClient.getClient();
        SessionManager sessionManager = new SessionManager(DetailActivity.this);
        RequestParam requestParam = new RequestParam();
        requestParam.setId(mCurrentEpisodeId);
        Call<TokenResult> service = apiClient.create(Service.class).removeFromFavourite(requestParam, sessionManager.getToken());
        service.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

                if (response.body() != null && response.body().isSuccess()) {
                    getUserContent();
                }

            }
            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {


            }
        });
    }
}
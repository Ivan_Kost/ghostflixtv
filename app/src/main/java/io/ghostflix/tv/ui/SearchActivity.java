
package io.ghostflix.tv.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import io.ghostflix.tv.R;
import io.ghostflix.tv.fragmentui.SearchFragment;


/*
 * SearchActivity for SearchFragment
 */
public class SearchActivity extends LeanbackActivity {
    private SearchFragment mFragment;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.s_s_fragment);
       /* mFragment = (SearchFragment) getSupportFragmentManager()
                .findFragmentById(R.id.search_fragment_1);*/
    }

    @Override
    public boolean onSearchRequested() {
        if (mFragment.hasResults()) {
            startActivity(new Intent(this, SearchActivity.class));
        } else {
            mFragment.startRecognition();
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // If there are no results found, press the left key to reselect the microphone
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT && !mFragment.hasResults()) {
            mFragment.focusOnSearch();
        }
        return super.onKeyDown(keyCode, event);
    }
}
package io.ghostflix.tv.model.video;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Files {

    @SerializedName("progressive")
    private List<Progressive> progressiveList;

    public List<Progressive> getProgressiveList() {
        return progressiveList;
    }
}

package io.ghostflix.tv.model.episode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class EpisodeResult {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("poster_image")
    @Expose
    private String posterImage;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("resolution")
    @Expose
    private String resolution;
    @SerializedName("header_image")
    @Expose
    private String headerImage;

    @SerializedName("header_background")
    @Expose
    private String header_background;
    @SerializedName("video_embed_trailer")
    @Expose
    private String videoEmbedTrailer;
    @SerializedName("video_embed_poster")
    @Expose
    private String videoEmbedPoster;
//    @SerializedName("comments")
//    @Expose
//    private List<Comment> comments = null;
    @SerializedName("genres")
    @Expose
    private List<Genre> genres = null;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("video_director")
    @Expose
    private List<Object> videoDirector = null;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("episodes")
    @Expose
    private Map<String, List<EpisodeContent>> episodes;
    @SerializedName("season_title")
    @Expose
    private String seasonTitle;
    @SerializedName("season_title_two")
    @Expose
    private String seasonTitleTwo;
    @SerializedName("season_title_three")
    @Expose
    private String seasonTitleThree;
    @SerializedName("is_locked")
    @Expose
    private Boolean locked;

    public Map<String, List<EpisodeContent>> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Map<String, List<EpisodeContent>> episodes) {
        this.episodes = episodes;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    @SerializedName("season_title_four")
    @Expose
    private String seasonTitleFour;
    @SerializedName("season_title_five")
    @Expose
    private String seasonTitleFive;
    @SerializedName("is_favorite")
    @Expose
    private Boolean isFavorite;
    @SerializedName("in_watchlist")
    @Expose
    private Boolean inWatchlist;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getPosterImage() {
        return posterImage;
    }

    public void setPosterImage(String posterImage) {
        this.posterImage = posterImage;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getHeaderImage() {
        return headerImage;
    }

    public void setHeaderImage(String headerImage) {
        this.headerImage = headerImage;
    }

    public String getVideoEmbedTrailer() {
        return videoEmbedTrailer;
    }

    public void setVideoEmbedTrailer(String videoEmbedTrailer) {
        this.videoEmbedTrailer = videoEmbedTrailer;
    }

    public String getVideoEmbedPoster() {
        return videoEmbedPoster;
    }

    public void setVideoEmbedPoster(String videoEmbedPoster) {
        this.videoEmbedPoster = videoEmbedPoster;
    }

//    public List<Comment> getComments() {
//        return comments;
//    }
//
//    public void setComments(List<Comment> comments) {
//        this.comments = comments;
//    }


    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<Object> getVideoDirector() {
        return videoDirector;
    }

    public void setVideoDirector(List<Object> videoDirector) {
        this.videoDirector = videoDirector;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }



    public String getSeasonTitle() {
        return seasonTitle;
    }

    public void setSeasonTitle(String seasonTitle) {
        this.seasonTitle = seasonTitle;
    }

    public String getSeasonTitleTwo() {
        return seasonTitleTwo;
    }

    public void setSeasonTitleTwo(String seasonTitleTwo) {
        this.seasonTitleTwo = seasonTitleTwo;
    }

    public String getSeasonTitleThree() {
        return seasonTitleThree;
    }

    public void setSeasonTitleThree(String seasonTitleThree) {
        this.seasonTitleThree = seasonTitleThree;
    }

    public String getSeasonTitleFour() {
        return seasonTitleFour;
    }

    public void setSeasonTitleFour(String seasonTitleFour) {
        this.seasonTitleFour = seasonTitleFour;
    }

    public String getSeasonTitleFive() {
        return seasonTitleFive;
    }

    public void setSeasonTitleFive(String seasonTitleFive) {
        this.seasonTitleFive = seasonTitleFive;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public Boolean getInWatchlist() {
        return inWatchlist;
    }

    public void setInWatchlist(Boolean inWatchlist) {
        this.inWatchlist = inWatchlist;
    }


    public String getHeader_background() {

        if(header_background.equalsIgnoreCase("")){
            return getPosterImage();
        }
        return header_background;
    }

    public void setHeader_background(String header_background) {
        this.header_background = header_background;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
}


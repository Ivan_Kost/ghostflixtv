package io.ghostflix.tv.model.video;

public class VimeoTextTrack {
    /*"uri": "/videos/445516131/texttracks/10623260",
"active": true,
"type": "subtitles",
"language": "de",
"display_language": "German",
"id": 10623260,
"link": "https://captions.cloud.vimeo.com/captions/10623260.vtt?expires=1665528840&sig=5558be47f3d5a54e0ad3d63f86739df8f95e84f2&download=Geisterakten_S01E01_GHOSTflix_Die+alte+Heimschule+am+Laacher+See.srt.vtt",
"link_expires_time": 1665528840,
"hls_link": "https://captions.cloud.vimeo.com/captions/10623260.vtt?expires=1665528840&sig=5558be47f3d5a54e0ad3d63f86739df8f95e84f2&download=Geisterakten_S01E01_GHOSTflix_Die+alte+Heimschule+am+Laacher+See.srt.vtt&hls=1",
"hls_link_expires_time": 1665528840,
"name": "Geisterakten_S01E01_GHOSTflix_Die alte Heimschule am Laacher See.srt"*/

    private String uri;
    private String active;
    private String type;
    private String language;
    private String display_language;
    private String id;
    private String link;
    private int link_expires_time;
    private String hls_link;
    private int hls_link_expires_time;
    private String name;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDisplay_language() {
        return display_language;
    }

    public void setDisplay_language(String display_language) {
        this.display_language = display_language;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getLink_expires_time() {
        return link_expires_time;
    }

    public void setLink_expires_time(int link_expires_time) {
        this.link_expires_time = link_expires_time;
    }

    public String getHls_link() {
        return hls_link;
    }

    public void setHls_link(String hls_link) {
        this.hls_link = hls_link;
    }

    public int getHls_link_expires_time() {
        return hls_link_expires_time;
    }

    public void setHls_link_expires_time(int hls_link_expires_time) {
        this.hls_link_expires_time = hls_link_expires_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package io.ghostflix.tv.model.video;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TextTracksApiResult {
    @SerializedName("data")
    private List<VimeoTextTrack> textTracks;

    public List<VimeoTextTrack> getTextTracks() {
        return textTracks;
    }
}

package io.ghostflix.tv.model.video;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoApiResult {
    @SerializedName("files")
    private List<VideoFile> files;

    public List<VideoFile> getFiles() {
        return files;
    }
}

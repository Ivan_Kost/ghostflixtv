package io.ghostflix.tv.model.content;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AWSUrls {

    @SerializedName("sdVideo")
    @Expose
    private String sdVideo;

    @SerializedName("hdVideo")
    @Expose
    private String hdVideo;

    @SerializedName("textTrack")
    @Expose
    private String textTrack;

    public String getSdVideo() {
        return sdVideo;
    }

    public void setSdVideo(String sdVideo) {
        this.sdVideo = sdVideo;
    }

    public String getHdVideo() {
        return hdVideo;
    }

    public void setHdVideo(String hdVideo) {
        this.hdVideo = hdVideo;
    }

    public String getTextTrack() {
        return textTrack;
    }

    public void setTextTrack(String textTrack) {
        this.textTrack = textTrack;
    }
}

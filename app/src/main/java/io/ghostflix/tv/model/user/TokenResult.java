package io.ghostflix.tv.model.user;

public class TokenResult {

    private String token;
    private String user_email;
    private String user_nicename;
    private String user_display_name;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public String getCode() {
        return code;
    }

    private String code;

    public String getToken() {
        return token;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_nicename() {
        return user_nicename;
    }

    public String getUser_display_name() {
        return user_display_name;
    }
}


package io.ghostflix.tv.model.video;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Request {

    @SerializedName("files")
    private Files files;
    @SerializedName("text_tracks")
    private List<TextTracks> textTracksList;

    public List<TextTracks> getTextTracksList() {
        return textTracksList;
    }

    public Files getFiles() {
        return files;
    }
}

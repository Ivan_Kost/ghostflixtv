package io.ghostflix.tv.model.video;

import io.ghostflix.tv.model.content.AWSUrls;

public class SignedUrlsResult {
    private AWSUrls signed_url;

    public AWSUrls getSigned_url() {
        return signed_url;
    }

    public void setSigned_url(AWSUrls signed_url) {
        this.signed_url = signed_url;
    }
}

package io.ghostflix.tv.model.video;

public class TextTracks {

    private String lang;
    private String url;
    private String kind;
    private String id;
    private String label;


    public String getLang() {
        return lang;
    }

    public String getUrl() {
        return url;
    }

    public String getKind() {
        return kind;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }
}

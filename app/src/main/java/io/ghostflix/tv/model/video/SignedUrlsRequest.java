package io.ghostflix.tv.model.video;

import io.ghostflix.tv.model.content.AWSUrls;

public class SignedUrlsRequest {
    private AWSUrls aws_url;

    public AWSUrls getAws_url() {
        return aws_url;
    }

    public void setAws_url(AWSUrls aws_url) {
        this.aws_url = aws_url;
    }
}

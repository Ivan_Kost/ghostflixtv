package io.ghostflix.tv.model.video;

import com.google.gson.annotations.SerializedName;

/**
 * deprecated
 */

public class PlayerConfigResult {

    @SerializedName("request")
    private Request request;
    public Request getRequest() {
        return request;
    }
}

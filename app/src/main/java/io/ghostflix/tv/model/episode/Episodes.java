package io.ghostflix.tv.model.episode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Episodes {

    @SerializedName("episodes")
    @Expose
    private List<EpisodeContent> episodes;

    public List<EpisodeContent> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<EpisodeContent> episodes) {
        this.episodes = episodes;
    }

}

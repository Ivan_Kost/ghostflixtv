package io.ghostflix.tv.model.video;

public class Progressive {

    private String profile;
    private String width;
    private String mime;
    private String fps;
    private String url;
    private String cdn;
    private String quality;
    private String id;
    private String origin;
    private String height;

    public String getProfile() {
        return profile;
    }

    public String getWidth() {
        return width;
    }

    public String getMime() {
        return mime;
    }

    public String getFps() {
        return fps;
    }

    public String getUrl() {
        return url;
    }

    public String getCdn() {
        return cdn;
    }

    public String getQuality() {
        return quality;
    }

    public String getId() {
        return id;
    }

    public String getOrigin() {
        return origin;
    }

    public String getHeight() {
        return height;
    }
}

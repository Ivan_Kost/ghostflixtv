package io.ghostflix.tv.model.episode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EpisodeContent {

    @SerializedName("episode_image")
    @Expose
    private String episodeImage;
    @SerializedName("vimeo_video")
    @Expose
    private String vimeoVideo;
    @SerializedName("episode_title")
    @Expose
    private String episodeTitle;
    @SerializedName("video_poster")
    @Expose
    private String videoPoster;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("media_duration_meta")
    @Expose
    private String mediaDurationMeta;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("force_subtitles")
    @Expose
    private String forceSubtitles;
    @SerializedName("mp4_video")
    @Expose
    private String mp4_video;
    @SerializedName("mp4_video_hd")
    @Expose
    private String mp4_video_hd;
    @SerializedName("textrack_vtt")
    @Expose
    private String textrack;
    @SerializedName("progress")
    @Expose
    private long progress;
    @SerializedName("skip_intro_time")
    @Expose
    private String skipIntroTime;

    @SerializedName("video_id")
    @Expose
    private String video_id;

    @SerializedName("is_watched")
    @Expose
    private boolean is_watched;

    @SerializedName("is_locked")
    @Expose
    private boolean locked;

    @SerializedName("time")
    @Expose
    private float time;

    @SerializedName("hls_video")
    @Expose
    private String hls_video;

    public void setProgress(long progress) {
        this.progress = progress;
    }

    public String getSkipIntroTime() {
        return skipIntroTime;
    }

    public void setSkipIntroTime(String skipIntroTime) {
        this.skipIntroTime = skipIntroTime;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getEpisodeImage() {
        return episodeImage;
    }

    public void setEpisodeImage(String episodeImage) {
        this.episodeImage = episodeImage;
    }

    public String getVimeoVideo() {
        return vimeoVideo;
    }

    public void setVimeoVideo(String vimeoVideo) {
        this.vimeoVideo = vimeoVideo;
    }

    public String getEpisodeTitle() {
        return episodeTitle;
    }

    public void setEpisodeTitle(String episodeTitle) {
        this.episodeTitle = episodeTitle;
    }

    public String getVideoPoster() {
        return videoPoster;
    }

    public void setVideoPoster(String videoPoster) {
        this.videoPoster = videoPoster;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getMediaDurationMeta() {
        return mediaDurationMeta;
    }

    public void setMediaDurationMeta(String mediaDurationMeta) {
        this.mediaDurationMeta = mediaDurationMeta;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getForceSubtitles() {
        return forceSubtitles;
    }

    public void setForceSubtitles(String forceSubtitles) {
        this.forceSubtitles = forceSubtitles;
    }

    public String getMp4_video() {
        return mp4_video;
    }

    public void setMp4_video(String mp4_video) {
        this.mp4_video = mp4_video;
    }

    public String getMp4_video_hd() {
        return mp4_video_hd;
    }

    public void setMp4_video_hd(String mp4_video_hd) {
        this.mp4_video_hd = mp4_video_hd;
    }

    public String getTextrack() {
        return textrack;
    }

    public void setTextrack(String textrack) {
        this.textrack = textrack;
    }

    public boolean isIs_watched() {
        return is_watched;
    }

    public Long getTime() {
        return (long) ((time * 1000));
    }

    public void setTime(long is_watched) {
        this.time = is_watched;
    }

    public long getProgress() {
        return progress;
    }

    public String getHls_video() {
        return hls_video;
    }

    public void setHls_video(String hls_video) {
        this.hls_video = hls_video;
    }

    public boolean isLocked(){
        return locked;
    }
}

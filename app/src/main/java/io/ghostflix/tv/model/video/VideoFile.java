package io.ghostflix.tv.model.video;

public class VideoFile {
    private String quality;
    private String rendition;
    private String type;
    private String width;
    private String height;
    private String link;
    private String created_time;
    private float fps;
    private long size;
    private String md5;
    private String public_name;
    private String size_short;

    public String getQuality() {
        return quality;
    }

    public String getRendition() {
        return rendition;
    }

    public String getType() {
        return type;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public String getLink() {
        return link;
    }

    public String getCreated_time() {
        return created_time;
    }

    public float getFps() {
        return fps;
    }

    public long getSize() {
        return size;
    }

    public String getMd5() {
        return md5;
    }

    public String getPublic_name() {
        return public_name;
    }

    public String getSize_short() {
        return size_short;
    }
}

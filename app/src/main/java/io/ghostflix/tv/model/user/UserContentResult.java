package io.ghostflix.tv.model.user;

import io.ghostflix.tv.model.content.VideoItem;

import java.util.List;

public class UserContentResult {


    private String id;
    private String name;
    private String avatar;
    private int favoriten_cnt;

    public String getName() {
        return name;
    }

    private int watch_list_cnt;
    private List<VideoItem> favoriten;
    private List<VideoItem> wishlist;
//    private String comments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getFavoriten_cnt() {
        return favoriten_cnt;
    }

    public void setFavoriten_cnt(int favoriten_cnt) {
        this.favoriten_cnt = favoriten_cnt;
    }

    public int getWatch_list_cnt() {
        return watch_list_cnt;
    }

    public void setWatch_list_cnt(int watch_list_cnt) {
        this.watch_list_cnt = watch_list_cnt;
    }

    public List<VideoItem> getFavoriten() {
        return favoriten;
    }

    public void setFavoriten(List<VideoItem> favoriten) {
        this.favoriten = favoriten;
    }

    public List<VideoItem> getWishlist() {
        return wishlist;
    }

    public void setWishlist(List<VideoItem> wishlist) {
        this.wishlist = wishlist;
    }

}

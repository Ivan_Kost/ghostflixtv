package io.ghostflix.tv.utils;

import static io.ghostflix.tv.utils.PlayerUtil.MIME_TYPE_EXTRA;
import static io.ghostflix.tv.utils.PlayerUtil.SUBTITLE_URI_EXTRA;
import static io.ghostflix.tv.utils.PlayerUtil.TITLE_EXTRA;

import android.content.Intent;

import com.google.android.exoplayer2.util.MimeTypes;
import io.ghostflix.tv.media.ExoPlayerActivity;
import io.ghostflix.tv.model.content.AWSUrls;
import io.ghostflix.tv.model.episode.EpisodeContent;
import io.ghostflix.tv.model.video.VideoApiResult;
import io.ghostflix.tv.ui.BaseActivity;


public class PlayerHelper {
    public static Intent createVideoIntent(EpisodeContent episodeContent, BaseActivity activity, String slug, AWSUrls awsUrls){

        Intent intent = new Intent(activity, ExoPlayerActivity.class);
        intent.putExtra("VideoTitle", episodeContent.getEpisodeTitle());
        intent.putExtra("VidDesc", episodeContent.getDescription());
        intent.putExtra("skipIntroTime",episodeContent.getSkipIntroTime());
        intent.putExtra(MIME_TYPE_EXTRA,"video/mp4");
        intent.putExtra(TITLE_EXTRA,episodeContent.getEpisodeTitle());
        //intent.putExtra("VideoLink", PictureUtils.getVideo(channelResponse.getRequest().getFiles().getProgressiveList()));
       /*  if(mp4UrlSource != null && !mp4UrlSource.equals("")) {
           try {
                InputStream derPrivateKeyIS = activity.getResources().openRawResource(R.raw.ghostflix_pk_2);
                InputStream testTextStream = activity.getResources().openRawResource(R.raw.test_text);
                InputStream testKeyStream = activity.getResources().openRawResource(R.raw.test);

                mp4Url = CFSignedUrl.createSignUrl(derPrivateKeyIS, mp4UrlSource);
                derPrivateKeyIS.close();
            } catch (Exception e) {

                Log.d("MSG", "Video link sign error!!!");
                e.printStackTrace();
                mp4Url = "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_640_3MG.mp4";
         }
    }*/
        intent.putExtra("VideoLink", awsUrls.getHdVideo());
        intent.putExtra("VideoSDLink", awsUrls.getSdVideo());
        intent.putExtra("VideoHDLink", awsUrls.getHdVideo());
        intent.putExtra("videoLinksArray",PictureUtils.getAWSArray(awsUrls));
        //episodeContent.getMp4_video()
        intent.putExtra("subTitleLink", awsUrls.getTextTrack());
        intent.putExtra(SUBTITLE_URI_EXTRA,awsUrls.getTextTrack());

        if(episodeContent.getForceSubtitles() != null && episodeContent.getForceSubtitles().equalsIgnoreCase("on")){
            intent.putExtra("force_subtitle",true);
        }
        intent.putExtra("slug",slug);
        intent.putExtra("video_id",episodeContent.getVideo_id());
       return intent;
    }

    public static Intent createVimeoIntent(EpisodeContent episodeContent, BaseActivity activity, String slug, long mResumeTime , VideoApiResult channelResponse){
        Intent intent = new Intent(activity, ExoPlayerActivity.class);
        intent.putExtra("VideoTitle", episodeContent.getEpisodeTitle());
        intent.putExtra("VidDesc", episodeContent.getDescription());
        intent.putExtra("VideoLink", PictureUtils.getApiVideo(channelResponse.getFiles()));
        intent.putExtra("videoLinksArray",PictureUtils.getVimeoArray(channelResponse.getFiles()));

        // intent.putExtra("VideoLink", episodeContent.getMp4_video());
        // intent.putExtra("subTitleLink", PictureUtils.getSubtitle(channelResponse.getRequest().getTextTracksList()));
        if (episodeContent.getForceSubtitles() != null && episodeContent.getForceSubtitles().equalsIgnoreCase("on")) {
            intent.putExtra("force_subtitle", true);
        }
        //   intent.putExtra("vimeoId", vimeoId);
        intent.putExtra("slug", slug);
        intent.putExtra("video_id", episodeContent.getVideo_id());
        intent.putExtra("skipIntroTime",episodeContent.getSkipIntroTime());
        intent.putExtra("resume",mResumeTime);
        return intent;
    }

    public static Intent createHLSVideoIntent(EpisodeContent episodeContent, BaseActivity activity, String slug, String hlsUrl, String subtitles){

        Intent intent = new Intent(activity, ExoPlayerActivity.class);
        intent.putExtra("VideoTitle", episodeContent.getEpisodeTitle());
        intent.putExtra("VidDesc", episodeContent.getDescription());
        intent.putExtra("skipIntroTime",episodeContent.getSkipIntroTime());
        intent.putExtra(MIME_TYPE_EXTRA, MimeTypes.APPLICATION_M3U8);
        intent.putExtra(TITLE_EXTRA,episodeContent.getEpisodeTitle());
        intent.putExtra("VideoHLSLink", hlsUrl);
        intent.putExtra("subTitleLink", subtitles);
        intent.putExtra(SUBTITLE_URI_EXTRA,subtitles);

        if(episodeContent.getForceSubtitles() != null && episodeContent.getForceSubtitles().equalsIgnoreCase("on")){
            intent.putExtra("force_subtitle",true);
        }
        intent.putExtra("slug",slug);
        intent.putExtra("video_id",episodeContent.getVideo_id());
        return intent;
    }
}

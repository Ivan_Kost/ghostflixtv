package io.ghostflix.tv.utils;

import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import io.ghostflix.tv.R;


public class SearchHelper {

    private final FragmentActivity activity;

    public SearchHelper(FragmentActivity fragmentActivity) {

        this.activity = fragmentActivity;
    }

    public void addKeyWordView(LinearLayout container, EditText editTextSearch) {
        container.setWeightSum(34.2f);
        String[] arr = activity.getResources().getStringArray(R.array.keyWords);
        for (int  i = 0; i<arr.length;i++){



            LayoutInflater local = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view1=  local.inflate(R.layout.text_keyword, null);
            LinearLayout.LayoutParams params;
            final TextView textView = view1.findViewById(R.id.tv_search_kayword);
            if (arr[i].equalsIgnoreCase("space")){

                params=new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,3.0f);
                params.gravity= Gravity.CENTER;
                textView.setGravity(Gravity.CENTER);
            }else if (arr[i].equalsIgnoreCase("delete")){
                params=new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,3.5f);
                params.gravity= Gravity.RIGHT|Gravity.CENTER_VERTICAL;
                textView.setGravity(Gravity.RIGHT);
            }else if (arr[i].equalsIgnoreCase("#")){
                params=new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,.7f);
                params.gravity= Gravity.RIGHT|Gravity.CENTER_VERTICAL;
                textView.setGravity(Gravity.RIGHT);
            }else{
                params=new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1.0f);
                params.gravity= Gravity.CENTER;
                textView.setGravity(Gravity.CENTER);
            }

            view1.setLayoutParams(params);

            textView.setText(arr[i]);
            textView.setTag(arr[i]);
            textView.setTextSize(17.f);


            textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {

                    if (b){
                        textView.setTextSize(22.5f);
                        textView.setTextColor(ContextCompat.getColor(activity, R.color.app_red));
                        if (textView.getTag().toString().equalsIgnoreCase("DELETE")) {
                        }
                    }else {
                        textView.setTextSize(17.f);
                        textView.setTextColor(ContextCompat.getColor(activity, R.color.lb_tv_white));
                    }
                }
            });
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (textView.getTag().toString().equalsIgnoreCase("DELETE")) {
                        int length = editTextSearch.getText().length();
                        if (length > 0) {
                            editTextSearch.getText().delete(length - 1, length);
                        }
                    } else if (textView.getTag().toString().equalsIgnoreCase("space")){
                        editTextSearch.append(" ");
                    } else{
                        editTextSearch.append(textView.getTag().toString());
                    }
                }
            });
            container.addView(textView);



        }

    }

    public void addNumKeyWordView(LinearLayout container, EditText editTextSearch) {
        String[] arr = activity.getResources().getStringArray(R.array.number);
        for (int  i = 0; i<arr.length;i++){
            LayoutInflater local = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view1=  local.inflate(R.layout.text_keyword, null);
            LinearLayout.LayoutParams params;
            final TextView textView = view1.findViewById(R.id.tv_search_kayword);
            if (arr[i].equalsIgnoreCase("space")){
                Resources res = activity.getResources();
                int width = res.getDimensionPixelSize(R.dimen.text_width);
                params=new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity= Gravity.CENTER;
                textView.setGravity(Gravity.CENTER);
            }else if (arr[i].equalsIgnoreCase("delete")){
                Resources res = activity.getResources();
                int width = res.getDimensionPixelSize(R.dimen.text_width);
                params=new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity= Gravity.RIGHT|Gravity.CENTER_VERTICAL;
                textView.setGravity(Gravity.RIGHT);
            }else{
                Resources res = activity.getResources();
                int width = res.getDimensionPixelSize(R.dimen.text_width);

                params=new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity= Gravity.CENTER;
                textView.setGravity(Gravity.CENTER);
            }

            view1.setLayoutParams(params);

            textView.setText(arr[i]);
            textView.setTag(arr[i]);
            textView.setTextSize(17.f);


            textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {

                    if (b){
                        textView.setTextSize(22.5f);
                        textView.setTextColor(ContextCompat.getColor(activity, R.color.app_red));
                        if (textView.getTag().toString().equalsIgnoreCase("DELETE")) {
                        }
                    }else {
                        textView.setTextSize(17.f);
                        textView.setTextColor(ContextCompat.getColor(activity, R.color.lb_tv_white));
                    }
                }
            });
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (textView.getTag().toString().equalsIgnoreCase("DELETE")) {
                        int length = editTextSearch.getText().length();
                        if (length > 0) {
                            editTextSearch.getText().delete(length - 1, length);
                        }
                    } else if (textView.getTag().toString().equalsIgnoreCase("space")){
                        editTextSearch.append(" ");
                    } else{
                        editTextSearch.append(textView.getTag().toString());
                    }
                }
            });
            container.addView(textView);



        }

    }

//    public List<ChannelDetail> getSearchResultList(String searchText, List<CategoryDetail> searchResult, String  param){
//
//        List<ChannelDetail> videoList = new ArrayList<>();
//
//        for (CategoryDetail categoryDetail: searchResult
//             ) {
//
//            if(categoryDetail.getCategoryName().equalsIgnoreCase("All")){
//                List<ChannelDetail> channelDetailList =  categoryDetail.getChannelDetails();
//                for (ChannelDetail channelDetail: channelDetailList
//                ) {
//                    if(channelDetail.getChannelName() != null && Pattern.compile(Pattern.quote(searchText), Pattern.CASE_INSENSITIVE).matcher(channelDetail.getChannelName()).find()){
//                        videoList.add(channelDetail);
//                    }
//                }
//            }
//
//        }
//
//        return  videoList;
//    }
//
//    public String getSearchResultCategoryId( List<CategoryDetail> searchResult){
//
//        String categoryId = "";
//        for (CategoryDetail categoryDetail: searchResult
//        ) {
//
//            if(categoryDetail.getCategoryName().equalsIgnoreCase("All")){
//                Log.d("TGAG", "getSearchResultCategoryId: "+ categoryDetail.getCategoryId().toString());
//                categoryId =   categoryDetail.getCategoryId().toString();
//            }
//
//        }
//        Log.d("TGAG", "getSearchResultCategoryId: "+ categoryId);
//        return  categoryId;
//    }


}

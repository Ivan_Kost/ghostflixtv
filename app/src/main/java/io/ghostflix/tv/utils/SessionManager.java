package io.ghostflix.tv.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;

import io.ghostflix.tv.model.user.TokenResult;

public class SessionManager {

    private final Context context;

    private static final String EMAIL = "email";
    private static final String DISPLAY_NAME = "display";
    private static final String NAME = "username";
    private static final String PRODUCT_APPNAME = "tv";
    private static final String TOKEN = "token";
    private static final String IS_LOGIN = "login";
    private final SharedPreferences pSharedPref ;
    private final SharedPreferences.Editor editor;

    public SessionManager(Context context) {
        this.context = context;
        if(context == null){
            pSharedPref = null;
            editor = null;
            return;
        }
        pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        editor = pSharedPref.edit();
    }

    public void saveLoginStatus(TokenResult tokenResult){


        editor.putString(EMAIL, tokenResult.getUser_email());
        editor.putString(DISPLAY_NAME, tokenResult.getUser_display_name());
        editor.putString(NAME, tokenResult.getUser_nicename());
        editor.putString(TOKEN, "Bearer "+tokenResult.getToken());
        editor.putBoolean(IS_LOGIN, true);
        editor.apply();
    }

    public  boolean getLoginStatus(){
        SharedPreferences pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        return pSharedPref.getBoolean(IS_LOGIN, false );
    }
    public String getEmail(){

        SharedPreferences pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        return pSharedPref.getString(EMAIL, "" );
    }
    public String getToken(){

        SharedPreferences pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        return pSharedPref.getString(TOKEN, "" );
    }

    public String getUserName(){
        SharedPreferences pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        return pSharedPref.getString(NAME, "" );
    }
    public String getDisplayName(){
        SharedPreferences pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        return pSharedPref.getString(DISPLAY_NAME, "" );
    }


    public  void clearSharedPref(){

        SharedPreferences pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pSharedPref.edit();
        editor.clear();
        editor.apply();
    }


    @SuppressLint("HardwareIds")
    public  String getDeviceId(Context context){
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public void saveResumeTime(String key, long time){

        editor.putLong(key, time);
        editor.commit();

    }

    public void setNotifyIsShown(){
        editor.putBoolean("notify_is_shown",true);
        editor.apply();
    }

    public boolean isNotifyShown(){
        SharedPreferences pSharedPref = context.getSharedPreferences(PRODUCT_APPNAME, Context.MODE_PRIVATE);
        return pSharedPref.getBoolean("notify_is_shown", false );
    }

}

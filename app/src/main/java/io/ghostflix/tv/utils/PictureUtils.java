package io.ghostflix.tv.utils;




import io.ghostflix.tv.model.content.AWSUrls;
import io.ghostflix.tv.model.video.Progressive;
import io.ghostflix.tv.model.video.TextTracks;
import io.ghostflix.tv.model.video.VideoFile;
import io.ghostflix.tv.model.video.VimeoTextTrack;

import java.util.ArrayList;
import java.util.List;

public class PictureUtils {

    public static  String getVideo(List<Progressive> videoItemList){

        String picture = "";

        for (Progressive item: videoItemList
        ) {
            if( item.getWidth().equalsIgnoreCase("1920")   ){
               return   item.getUrl();
            }
        }

        for (Progressive item: videoItemList
        ) {
            if( item.getWidth().equalsIgnoreCase("1280")   ){
                return   item.getUrl();
            }
        }

        for (Progressive item: videoItemList
        ) {
            if( item.getWidth().equalsIgnoreCase("960")   ){
                return   item.getUrl();
            }
        }
        for (Progressive item: videoItemList
        ) {
            if( item.getWidth().equalsIgnoreCase("640")   ){
                return   item.getUrl();
            }
        }


        return  picture;
    }

    public static String[] getAWSArray(AWSUrls awsUrls) {
        ArrayList<String> items = new ArrayList<>();
        String sd = awsUrls.getSdVideo();
        String hd = awsUrls.getHdVideo();
        if( sd != null &&  !sd.equals("")){
            items.add(sd);
        }
        if( hd != null && !hd.equals("")){
            items.add(hd);
        }
        return items.toArray(new String[0]);
    }

    public static String[] getVimeoArray(List<VideoFile> videoItemList){
        ArrayList<String> items = new ArrayList<>();
        for (VideoFile item: videoItemList
        ) {
            if( item.getWidth() != null && item.getWidth().equalsIgnoreCase("1920")   ){
                items.add( item.getLink());
            }
        }

        for (VideoFile item: videoItemList
        ) {
            if( item.getWidth() != null &&  item.getWidth().equalsIgnoreCase("1280")   ){
                items.add( item.getLink());
            }
        }

        for (VideoFile item: videoItemList
        ) {
            if( item.getWidth() != null &&  item.getWidth().equalsIgnoreCase("960")   ){
                items.add(item.getLink());
            }
        }
        for (VideoFile item: videoItemList
        ) {
            if (item.getWidth() != null &&  item.getWidth().equalsIgnoreCase("640")   ){
                items.add(item.getLink());
            }
        }
        return items.toArray(new String[0]);
    }

    public static  String getApiVideo(List<VideoFile> videoItemList){

        String picture = "";

        for (VideoFile item: videoItemList
        ) {
            if( item.getWidth() != null && item.getWidth().equalsIgnoreCase("1920")   ){
                return   item.getLink();
            }
        }

        for (VideoFile item: videoItemList
        ) {
            if( item.getWidth() != null && item.getWidth().equalsIgnoreCase("1280")   ){
                return   item.getLink();
            }
        }

        for (VideoFile item: videoItemList
        ) {
            if( item.getWidth() != null && item.getWidth().equalsIgnoreCase("960")   ){
                return   item.getLink();
            }
        }
        for (VideoFile item: videoItemList
        ) {
            if( item.getWidth() != null && item.getWidth().equalsIgnoreCase("640")   ){
                return   item.getLink();
            }
        }


        return  picture;
    }

    /**
     * @deprecated
     * @param textTracksList
     * @return
     */
    public static  String getSubtitle(List<TextTracks> textTracksList){

        String subTitle = null;

        if(textTracksList!= null && textTracksList.size() > 0){
            TextTracks textTracks  = textTracksList.get(0);
            subTitle = "https://player.vimeo.com"+textTracks.getUrl();
        }

        return  subTitle;
    }

    public static  String getTextTrack(List<VimeoTextTrack> textTracksList){

        String subTitle = null;
        if(textTracksList!= null && textTracksList.size() > 0){
            for (VimeoTextTrack textTracks : textTracksList) {
                String lang = textTracks.getLanguage();
                if(lang.equals("de")){
                    subTitle = textTracks.getLink();
                }
            }
        }

        return  subTitle;
    }
}

package io.ghostflix.tv.presenter;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.leanback.widget.ListRowPresenter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.RowHeaderPresenter;
import androidx.leanback.widget.RowHeaderView;

import io.ghostflix.tv.R;


public class CustomListRowPresenter extends ListRowPresenter {


    public CustomListRowPresenter(int focusZoomFactor, boolean useFocusDimmer) {
        super(focusZoomFactor, useFocusDimmer);
    }

    public void setSpiritColors(){

        setHeaderPresenter(new CustomRowHeaderPresenter());
    }


    @Override
    public boolean isUsingDefaultListSelectEffect() {
        return false;
    }

    @Override
    public boolean isUsingDefaultShadow() {
        return false;
    }

    public class CustomRowHeaderPresenter extends RowHeaderPresenter {
        @Override
        public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
            Presenter.ViewHolder viewHolder = super.onCreateViewHolder(parent);
            LinearLayout rowHeaderView = (LinearLayout) viewHolder.view;
            TextView title = viewHolder.view.findViewById(R.id.row_header);
            title.setTextColor( viewHolder.view.getResources().getColor(R.color.spirit_purple));
            return viewHolder;
        }
    }
}

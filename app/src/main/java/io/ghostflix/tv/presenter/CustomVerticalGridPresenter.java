package io.ghostflix.tv.presenter;

import android.util.Log;
import android.view.Gravity;
import android.view.View;

import androidx.leanback.widget.FocusHighlight;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.VerticalGridPresenter;
import androidx.leanback.widget.VerticalGridView;

import io.ghostflix.tv.R;

public class CustomVerticalGridPresenter extends VerticalGridPresenter {
    private ViewHolder vh;

    public CustomVerticalGridPresenter() {
        this(FocusHighlight.ZOOM_FACTOR_LARGE);
        setShadowEnabled(false);
    }

    public CustomVerticalGridPresenter(int focusZoomFactor) {
        super(focusZoomFactor, false);
    }
    public CustomVerticalGridPresenter(int focusZoomFactor, boolean useFocusDimmer) {
        super(focusZoomFactor, useFocusDimmer);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        super.onBindViewHolder(viewHolder, item);
        vh = (ViewHolder) viewHolder;

    }

    public void setRequestFocus(){
        vh.getGridView().requestFocus();
    }

    public int getId(){
        return vh.getGridView().getId();

    }
    public  void setScrollToPosition(int mInitialSelectedPosition){
        Log.d("TAG", "setScrollToPosition: ");
        vh.getGridView().setSelectedPosition(mInitialSelectedPosition, View.OVER_SCROLL_IF_CONTENT_SCROLLS);
    }

    @Override
    protected void initializeGridViewHolder(ViewHolder vh) {
        super.initializeGridViewHolder(vh);
        VerticalGridView gridView = vh.getGridView();
        gridView.setHorizontalSpacing(40);
        gridView.setVerticalSpacing(40);
        gridView.setGravity(Gravity.LEFT);
        int padding =40;
        gridView.setPadding(padding, padding, padding, padding);
        gridView.setNextFocusUpId(R.id.et_search);

    }

}


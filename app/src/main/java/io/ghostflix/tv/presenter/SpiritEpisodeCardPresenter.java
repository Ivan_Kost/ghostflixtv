package io.ghostflix.tv.presenter;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.leanback.widget.ImageCardView;
import androidx.leanback.widget.Presenter;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.episode.EpisodeContent;

public class SpiritEpisodeCardPresenter extends Presenter {
    private int mSelectedBackgroundColor = -1;
    private int mDefaultBackgroundColor = -1;
    private Drawable mDefaultCardImage;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        mDefaultBackgroundColor =
                ContextCompat.getColor(parent.getContext(), R.color.black_semitransparent);
        mSelectedBackgroundColor =
                ContextCompat.getColor(parent.getContext(), R.color.spirit_purple);
        mDefaultCardImage = parent.getResources().getDrawable(R.drawable.thumb, null);

        ImageCardView cardView = new ImageCardView(parent.getContext()) {
            @Override
            public void setSelected(boolean selected) {
                updateCardBackgroundColor(this, selected);
                super.setSelected(selected);
            }
        };

        cardView.setFocusable(true);
        cardView.setFocusableInTouchMode(true);
        cardView.getMainImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
        updateCardBackgroundColor(cardView, false);
        return new ViewHolder(cardView);
    }

    private void updateCardBackgroundColor(ImageCardView view, boolean selected) {
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;
        view.setBackgroundColor(color);
        view.findViewById(R.id.info_field).setBackgroundColor(color);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        EpisodeContent video = (EpisodeContent) item;

        ImageCardView cardView = (ImageCardView) viewHolder.view;
        cardView.setTitleText(video.getEpisodeTitle());
        cardView.setContentText(video.getMediaDurationMeta());

        if( !video.isLocked()){
            if(video.isIs_watched()){
                cardView.setBadgeImage(cardView.getContext().getDrawable(R.mipmap.ic_done));
            }else{
                cardView.setBadgeImage(cardView.getContext().getDrawable(R.drawable.ic_action_play_arrow));
            }
        }else{
            cardView.setBadgeImage(cardView.getContext().getDrawable(R.drawable.ic_lock_g));
        }




        // Set card size from dimension resources.
        Resources res = cardView.getResources();
        int width = res.getDimensionPixelSize(R.dimen.card_width);
        int height = res.getDimensionPixelSize(R.dimen.card_height);
        cardView.setMainImageDimensions(width, height);
        Glide.with(cardView.getContext())
                .load(video.getEpisodeImage())
                .apply(RequestOptions.errorOf(mDefaultCardImage))
                .into(cardView.getMainImageView());

    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        ImageCardView cardView = (ImageCardView) viewHolder.view;

        // Remove references to images so that the garbage collector can free up memory.
        cardView.setBadgeImage(null);
        cardView.setMainImage(null);
    }

    public void addLockIcon(){

    }

}
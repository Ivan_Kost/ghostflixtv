package io.ghostflix.tv.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import io.ghostflix.tv.R;

public class SpiritCategoryAdapter extends CategoryAdapter{
    public SpiritCategoryAdapter(List<String> categorylist, Activity homeActivity, OnTopRowClickListener listener) {
        super(categorylist, homeActivity, listener);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.top_spirit_layout, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(itemView);
        this.rv = itemView.findViewById(R.id.top_cat_lay);
        return viewHolder;
    }
}
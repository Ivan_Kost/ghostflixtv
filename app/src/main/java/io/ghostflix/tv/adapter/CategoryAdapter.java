package io.ghostflix.tv.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import io.ghostflix.tv.R;

import java.util.List;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder>
{
    private final List<String> categoryName;
    private OnTopRowClickListener listener = null;
    RelativeLayout rv;

    public CategoryAdapter(List<String> categorylist, Activity homeActivity, OnTopRowClickListener listener) {
        this.categoryName = categorylist;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.top_layout, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(itemView);
        this.rv = itemView.findViewById(R.id.top_cat_lay);
        return viewHolder;
    }

    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final String data = categoryName.get(position);

        if(data.equalsIgnoreCase("Se")){
            holder.title.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_action_search_off, 0, 0);
        }else if (data.equalsIgnoreCase("Pro")){
            holder.title.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_action_person_outline, 0, 0);
        }
        holder.title.setText(data);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(categoryName.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryName.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.categoryName);

        }
    }

    public interface OnTopRowClickListener {
        void onItemClick(String selectedText);
    }

    public void adjustCatogoryLayout(String category){
        Log.d("TAG", "adjustCatogiryLayoutr set Spirit bgt ");
        this.rv.setBackgroundResource(R.drawable.button_spirit_category_selector);

        if(category.equals("spirit") && this.rv != null){
            this.rv.setBackgroundResource(R.drawable.button_spirit_category_selector);
        }
    }
}


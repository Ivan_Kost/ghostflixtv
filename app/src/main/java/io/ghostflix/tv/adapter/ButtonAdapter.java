package io.ghostflix.tv.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import io.ghostflix.tv.R;

import java.util.List;


public class ButtonAdapter extends RecyclerView.Adapter<ButtonAdapter.MyViewHolder>
{
    private final List<String> categoryName;
    private OnTopRowClickListener listener = null;
    private final Activity detailActivity;
    public ButtonAdapter(List<String> categorylist, Activity homeActivity, OnTopRowClickListener listener) {
        this.categoryName = categorylist;
        this.listener = listener;
        detailActivity  = homeActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.button_layout, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final String data = categoryName.get(position);

        Log.d("ButtonAdapter", "onBindViewHolder: "+data);
        holder.title.setVisibility(View.GONE);
        holder.imageView.setVisibility(View.VISIBLE);

        if(data.equalsIgnoreCase("Add to Watchlist")){
            holder.imageView.setImageResource(R.drawable.ic_action_playlist_add);
        }else if(data.equalsIgnoreCase("Add to Favourite")){
            holder.imageView.setImageResource(R.drawable.ic_action_favorite);
        }else if(data.equalsIgnoreCase("Remove from Watchlist")){
            holder.imageView.setImageResource(R.drawable.ic_action_remove_done);
        }else if(data.equalsIgnoreCase("Remove to Favourite")){
            holder.imageView.setImageResource(R.drawable.ic_action_favorite_add);
        }
        else{
            holder.title.setVisibility(View.VISIBLE);
            Drawable img = detailActivity.getResources().getDrawable(R.drawable.ic_action_play_arrow);
            img.setBounds(0, 0, 60, 60);
            holder.title.setCompoundDrawables(img, null, null, null);
            holder.imageView.setVisibility(View.GONE);
        }

        holder.title.setText(data);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(categoryName.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryName.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.categoryName);
            imageView = view.findViewById(R.id.icon_button);

        }
    }

    public interface OnTopRowClickListener {
        void onItemClick(String selectedText);
    }

}


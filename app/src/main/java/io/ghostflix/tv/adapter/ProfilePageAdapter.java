package io.ghostflix.tv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.ProfileInfo;

import java.util.List;

public class ProfilePageAdapter extends RecyclerView.Adapter<ProfilePageAdapter.ProfileVH> {

    private final List<ProfileInfo> mProfileInfoList;


    public ProfilePageAdapter(List<ProfileInfo> profileInfoList) {
        this.mProfileInfoList = profileInfoList;
    }

    @Override
    public ProfileVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_info_list, parent, false);

        return new ProfileVH(itemView);
    }

    @Override
    public void onBindViewHolder(final ProfileVH holder, int position) {
        final ProfileInfo sectionGenreItemBean = mProfileInfoList.get(position);

        holder.textView.setText(sectionGenreItemBean.getTitle());
        holder.imageView.setImageResource(sectionGenreItemBean.getIcon());

        holder.relativeLayout.setOnFocusChangeListener((view, hasFocus) -> {
            Context context = holder.itemView.getContext();
            if (hasFocus) {
                Animation anim = AnimationUtils.loadAnimation(context, R.anim.sacle_up);
                holder.relativeLayout.startAnimation(anim);
                anim.setFillAfter(true);
                holder.relativeLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.background));
                holder.textView.setTextColor(ContextCompat.getColor(context, R.color.app_red));
            } else {
                Animation anim = AnimationUtils.loadAnimation(context, R.anim.scale_down);
                holder.relativeLayout.startAnimation(anim);
                anim.setFillAfter(true);
                holder.relativeLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.background));
                holder.textView.setTextColor(ContextCompat.getColor(context, R.color.app_red));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProfileInfoList.size();
    }

    class ProfileVH extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;
        RelativeLayout relativeLayout;

        ProfileVH(View view) {
            super(view);

            textView = view.findViewById(R.id.tv_profile_title_info);
            imageView = view.findViewById(R.id.img_profile_icon);
            relativeLayout = view.findViewById(R.id.rl_profile_list);
        }
    }
}

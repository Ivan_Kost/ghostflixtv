package io.ghostflix.tv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import io.ghostflix.tv.R;
import io.ghostflix.tv.model.episode.Genre;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;

import java.util.List;


public class CastRowAdapter extends RecyclerView.Adapter<CastRowAdapter.MyViewHolder>
{
    private final List<Genre> categoryName;
    private final Context context;

    public CastRowAdapter(List<Genre> categorylist, Context homeActivity) {
        this.categoryName = categorylist;
        this.context = homeActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cast_row, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Genre data = categoryName.get(position);
        holder.title.setText(data.getName());
        Glide.with(context)
                .load(data.getPhoto())
                .transform(new CircleCrop())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return categoryName.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView imageView;
        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.textView);
            imageView = view.findViewById(R.id.imageView);

        }
    }
}

